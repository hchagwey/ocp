#!/usr/bin/env bash
set -e

# Defaults
force_start=false
ignore_errors=false

usage()
{
  echo "Integrated Development Launcher for Environment"
  echo
  echo "usage: $0"
  echo "           start IDE"	
  echo
  echo "usage: $0 [-h] [-F] [-i]"
  echo
  echo "optional arguments:"
  echo "  -h       show this help message and exit"
  echo "  -F       force launch (get-deps)"
  echo "  -i       ignore errors"
  exit 0
}

while getopts "Fih" optionName
do
  case "$optionName" in
    F) force_start=true;;
    i) ignore_errors=true;;
    h | *) usage;;
  esac
done

# Determine OS/architecture
arch=$(uname | tr '[:upper:]' '[:lower:]')
echo "Current architecture: ${arch}"

# If depenencies are missing download them now so that lb can be used later in the script
if [[ ! -d upstream/gbm || ! -d upstream/logicblox || ! -d upstream/starter_planning_app ]]
then
  echo
  echo "Depenencies are missing. Downloading"
  ./scripts/get-deps.sh
  # no need to re-run this later in the script
  force_start=false
fi

source scripts/env.sh

# Determine lb version
echo
echo "Current lb version: $(lb -v)"

echo
echo "Kill IDE"
kill $(ps aux | grep 'modeler-configuration-tool' | awk '{print $2}') 2>/dev/null || : 

echo
echo "Delete all workspaces"
lb workspaces | while read line; do
  lb delete --force $line || $ignore_errors
done

echo
echo "Stop services"
lb services stop || $ignore_errors

echo
echo "Clear workspaces directory"
rm -Rf ${LB_DEPLOYMENT_HOME:-~/lb_deployment}/workspaces/* || $ignore_errors

if $force_start; then
  echo
  echo "Getting dependencies"
  ./scripts/get-deps.sh
  source scripts/env.sh
else
  echo
  echo "Start services"
  lb services start || $ignore_errors
fi

echo
echo "Restart gbm"
bash ${GBM_HOME}/gbm.sh restart ${APP_HOME}/config/gbm.config

lb config
if [[ $arch == 'linux' ]]; then
    ./upstream/logicblox/ide/Modeler\ Configuration\ Tool-1.0.0-linux-x86_64.AppImage
else
    open ./upstream/logicblox/ide/Modeler\ Configuration\ Tool.app
fi
exit 0
