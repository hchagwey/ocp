#! /usr/bin/env bash

set -ex

#
# Default settings and getopts
#
bootstrap="no"
do_nothing="no"
workflow="no"
inflate_ws="no" 

uncompress_workspaces()
{
  for i in `ls $1/*gz` ; do echo $i|awk -F'.tgz' -v file_name="$1" '{print $1}' ; done | xargs rm -rf
  for i in `ls $1/*gz` ; do echo $i|awk -F'.tgz' -v file_name="$1" '{print $1}' ; done | xargs mkdir
  for i in `ls $1/*gz`
   do ws=$i
   echo $ws
   target=`echo $ws | awk -F'.tgz' -v file_name="$1"  '{print $1}'`
   tar xvfz $ws -C $target
 done
}

uncompress()
{
  echo "START: uncompressing workspaces from closure"
  mkdir -p ${LB_DEPLOYMENT_HOME}/uncompressed-workspaces
  rm -rf   {LB_DEPLOYMENT_HOME}/uncompressed-workspaces/*
  cp -r $(dirname $(readlink -f $0))/workspaces/* ${LB_DEPLOYMENT_HOME}/uncompressed-workspaces
  chmod 775 -R ${LB_DEPLOYMENT_HOME}/uncompressed-workspaces
  uncompress_workspaces ${LB_DEPLOYMENT_HOME}/uncompressed-workspaces
  echo "END: uncompressing workspaces from closure"
}               


while getopts "bwzi" optionName
do
    case "$optionName" in
        b) bootstrap="yes";;
        w) workflow="yes";;
        z) do_nothing="yes";;
        i) inflate_ws="yes";;
    esac
done

if [ "${do_nothing}" == "yes" ]; then
    exit 0
fi

#
# Uncompress workspaces
#
if [ "${inflate_ws}" == "yes" ]; then
	uncompress
fi

#
# Import new workflow workspace
#

if [ "${workflow}" == "yes" ]; then
    lb web-server unload-services -w /workflow
    lb import-workspace --overwrite "/workflow" "/data/lb_deployment/uncompressed-workspaces/workflow"
    lb web-server load-services -w /workflow
fi

#
# Start bootstrap workflow
# bootstrap-gbm, bootstrap-master, and bootstrap-stage
#
if [ "${bootstrap}" == "yes" ]; then
    lb exec /workflow -f /etc/logicblox/workflowConfig.logic
    lb workflow start --name bootstrap_app
fi

exit 0
