# This is an auto-generated file. Do no override it.

{ nixpkgs ? <nixpkgs>
    , platform_release
    , src_modeler_app
    , doCheck ? true
    , buildParams ? ""
    }:
    let
      builder_config = import <config> { inherit nixpkgs; };
    in
      builder_config.genericAppJobset {
        logicblox = builder_config.getLB platform_release;
    
        build =
            builder_config.buildIDEWorkspace {
                inherit platform_release;
                src=src_modeler_app;
                projectName="OCP";
                workspaceName = "/ocp proxy";
                ihnerit buildParams;
            };
      }
