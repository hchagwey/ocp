
#! /usr/bin/env bash

set -e
set -x

source install_variables.sh

lb import-workspace --overwrite $workspace workspace_export/

lb exec $workspace '^measure:admin:allowed[]="enabled".'

lb web-server load-services -w $workspace

lb import-workspace --overwrite /workflow workflow_workspace/

lb web-server load-services -w /workflow

lb measure-service admin refresh -u http://localhost:55183$app_prefix/measure

if [ ./workflows/on_build.wf ]
then
    lb workflow run -q --file ./workflows/on_build.wf --param app_prefix="$appPrefix" --param location="$data_location" --param timeout="60s" --max-retries 2 
fi
