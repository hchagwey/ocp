#!/usr/bin/env python

from __future__ import print_function
import os
import os.path
import csv
import gzip
import glob
import argparse
import traceback
import subprocess
from datetime import datetime
from timeit import default_timer as timer

#config section
inputHierarchyFiles = ['00_time.dat', '01_product.dat', '02_location.dat']
inputBackupFiles = [
  'file_bop_merch_BOPMerch_TY_C_INB.dat', 
  'file_bop_undelivered_BOPUndeliver_TY_C_INB.dat',
  'file_chargeback_Chargeback_TY_R_INB.dat',
  'file_comp_Comp_TY_B_INB.dat',
  'file_onorder_OnordVendor_TY_C_INB.dat',
  'file_receipts_Rcpt_TY_C_INB.dat',
  'file_rtv_RTV_TY_C_INB.dat',
  'file_sales_dropship_SlsDropShip_TY_C_INB.dat',
  'file_sales_dropship_SlsDropShip_TY_R_INB.dat',
  'file_sales_net_promo_SlsNetPromo_TY_C_INB.dat',
  'file_sales_net_promo_SlsNetPromo_TY_R_INB.dat',
  'file_shrink_Shrink_TY_C_INB.dat'
]

inputIncomingFiles = [
  'bop_undelivered.dat', 
  'bop_merch.dat', 
  'chargeback.dat', 
  'onorder.dat', 
  'rtv.dat', 
  'receipts.dat',
  'shrink.dat', 
  'sales_net.dat',
  'sales_dropship.dat', 
  'sales_gross.dat'
]


#transformation rules
def obfuscateValue(fieldHeader):
  return { #backup/restore format, incomings format
    #02_location.dat (Chain|ChainLabel|Channel|ChannelLabel|Country|CountryLabel|Store|StoreLabel|frnchse|frnchseLabel|ttlfrnchse|ttlfrnchseLabel)
    #02_location.dat (Store|StoreLabel|Country|CountryLabel|Channel|ChannelLabel|MerchChannel|MerchChannelLabel|Chain|ChainLabel|frnchse|frnchseLabel|ttlfrnchse|ttlfrnchseLabel)
    'Channel': lambda val, row: transformIntValue(val, 'CH', 'CHANNEL_'),
    'ChannelLabel': lambda val, row: replaceValue(val),
    'Store': lambda val, row: transformIntValue(val, 'ST', 'STORE_', 300999),
    'StoreLabel': lambda val, row: generateIntValue(transformIntValue.prevValue, 'Store #'), #was: transformIntValue(val, '', 'Store #', 300999, '-') #TODO: add dictionary
    #01_product.dat (Brand|BrandLabel|Class|ClassLabel|Company|CompanyLabel|Department|DepartmentLabel|Sku|SkuLabel|Subclass|SubclassLabel)
    #01_product.dat (Sku|SkuLabel|Subclass|SubclassLabel|Class|ClassLabel|Department|DepartmentLabel|Brand|BrandLabel|Company|CompanyLabel|ProductMgr|ProductMgrLabel)
    'Brand': lambda val, row: transformIntValue(val, 'BR', 'DIV_'),
    'BrandLabel': lambda val, row: generateIntValue(transformIntValue.prevValue, 'Division #'),
    'Class': lambda val, row: transformIntValue(val, 'CL0', 'CLASS_', 99999),
    'ClassLabel': lambda val, row: generateIntValue(transformIntValue.prevValue, 'Class #'),
    'Company': lambda val, row: transformIntValue(val, 'CO', 'BRAND_'),
    'CompanyLabel': lambda val, row: generateIntValue(transformIntValue.prevValue, 'Brand #'),
    'Department': lambda val, row: transformIntValue(val, 'DP0', 'DEP_', 999),
    'DepartmentLabel': lambda val, row: generateIntValue(transformIntValue.prevValue, 'Department #'),
    'Sku': lambda val, row: generateIntValue(999999 - row, 'SKU_', formatStr='{:06d}'), #hashIntValue(val, 'SK', 'SKU_', 8),
    'SkuLabel': lambda val, row: generateIntValue(999999 - row, 'SKU #'), #hashIntValue.prevValue
    'Subclass': lambda val, row: transformIntValue(val, 'SC0', 'SUBC_', 9999999),
    'SubclassLabel': lambda val, row: generateIntValue(transformIntValue.prevValue, 'Subclass #')
    #00_time.dat (Month|MonthLabel|Quarter|QuarterLabel|Season|SeasonLabel|Week|WeekLabel|Year|YearLabel)
    #00_time.dat (Week|WeekLabel|Month|MonthLabel|Quarter|QuarterLabel|Season|SeasonLabel|Year|YearLabel|MerchMonth|MerchMonthLabel|MerchQuarter|MerchQuarterLabel|MerchSeason|MerchSeasonLabel|MerchYear|MerchYearLabel)
    #there no need to obfuscate calendar values and all changes in date format will be done with the transformator script
  }.get(fieldHeader, lambda val, row: val)


#value: <skipStart><intValue><skipEnd>[unusedTrailer]
#return: <addStart><newValue>[addEnd]
def transformIntValue(value, skipStart, addStart, maxValue=0, skipEnd='', addEnd=''):
  intValue = 0
  try:
    if skipStart: 
      parts = value.split(skipStart, 1)
      value = parts[1]
    if skipEnd:
      parts = value.split(skipEnd, 1)
      intValue=int(parts[0])            
    else:
      intValue = int(value)
  except (ValueError, IndexError):
    print ('ERROR in transformIntValue({}, \'{}\', {})'.format(value, skipStart, maxValue))
    return 'ERROR'
  
  if maxValue:
    newValue = maxValue - intValue
  else:
    newValue = intValue
  transformIntValue.prevValue = newValue
  return addStart + str(newValue) + addEnd


def hashIntValue(value, skipStart, addStart, numOfDigits):
  intValue = 0
  try:
    parts = value.split(skipStart, 1)
    intValue = abs(hash(parts[1])) % (10 ** numOfDigits)
  except (ValueError, IndexError):
    print ('ERROR in hashIntValue({}, \'{}\', {})'.format(value, skipStart))
    return 'ERROR'
  hashIntValue.prevValue = intValue
  return addStart + str(intValue)


def generateIntValue(intValue, addStart='', addEnd='', formatStr='{}'):
  return addStart + formatStr.format(intValue) + addEnd


def replaceValue(value):
  return {
    '1. Retail' : 'Retail',
    '2. DM' : 'DM',
    '3. Company Owned Retail' : 'Own',
    '4. Whs' : 'Warehouse'
  }.get(value, value)


#sample usage: python obfuscator.py --org s3folder --out s3folder
parser = argparse.ArgumentParser(description = '')
parser.add_argument('-org', '--orgDir', dest='orgDir', help="subdir's name with original Data set", default='')
parser.add_argument('-out', '--outDir', dest='outDir', help="subdir's name with obfuscated Data set", default='')
parser.add_argument('-kd', '--keyDir', dest='keyDir', help="param for cloud-store", default='/run/keys/')
parser.add_argument('-incs', '--incSrc', action='store_true', dest='incSrc', help='', default = False)
parser.add_argument('-hiers', '--hiersOnly', action='store_true', dest='hiersOnly', help="", default = False)
parser.add_argument('-r', '--report', dest='repFile', help="name of report file with summary information", default='obfuscator.log')
parser.add_argument('-v', '--verbose', action='store_true', dest='verbose', help='print more status messages to stdout', default = False)
args = parser.parse_args()


columnDicts = {}  #dictionaries for each column
#columnName : {orgValue : newObfuscatedValue}
def obfuscateHierarchyFile(fileName, outExt):
  totalLines = 0
  orgData = []

  with open(fileName) as orgf:
    orgData=orgf.readlines()
 
  #get header's fields
  headers = orgData[0].strip('\n').split("|")
  for header in headers:
    columnDicts[header] = {}

  delimiter = '|'  
  with open(fileName + outExt, 'w+') as of:
    of.write(orgData.pop(0)) # '\n'
    for line in orgData:
      orgLineList = line.strip('\n').split("|")
      index = 0
      newLine=[]
      for orgValue in orgLineList:
        #obfuscate each column values
        newValue = obfuscateValue(headers[index])(orgValue, totalLines + 1)
        newLine.append(newValue)
        columnDicts[headers[index]][orgValue] = newValue
        index += 1
      of.write(delimiter.join(newLine) + '\n')
      totalLines += 1

    #if args.verbose:
    #  print(fileName + ' RAM END: ' + psutil.virtual_memory())  #physical memory usage

    #save dictionaries
    for header in headers:
      with open('{}.dict'.format(header), 'w') as df: #fileName.split('.', 1)[0]
      #  df.write(str(columnDicts[header]))
        csvf = csv.writer(df, delimiter='|')
        for key, val in columnDicts[header].items():
          csvf.writerow([key, val])
      # check for duplicates
      checkdict = {}
      for key, value in columnDicts[header].items():
        checkdict.setdefault(value, set()).add(key)
      duplicates = [key for key, values in checkdict.items() if len(values) > 1]
      if duplicates:
        print('  ERROR: duplicate values were found in the {} dict: {} of {}\n{}'.format(header, len(duplicates), len(columnDicts[header]), duplicates))      
    
  return totalLines

#change all values using dictionaries in columns which were found in columnDicts 
def obfuscateDataFile(fileName, outExt):
  totalLines = 0
  takenLines = 0
  orgData = []

  with open(fileName) as orgf:
    orgData=orgf.readlines()
 
  #get header's fields
  headers = orgData[0].strip('\n').split("|")
  #define replacement dictionaries for each column
  dicts = [None] * len(headers)
  for pos, header in enumerate(headers):
    dicts[pos] = columnDicts.get(header, None)

  delimiter = '|'  
  with open(fileName + outExt, 'w+') as of:
    of.write(orgData.pop(0)) # '\n'
    missingKeys = set()
    missingKeysLinesCount = 0
    for line in orgData:
      totalLines += 1
      orgLineList = line.strip('\n').split("|")
      newLine=[]
      validLine = True
      for index, orgValue in enumerate(orgLineList):
        dictionary = dicts[index]
        if dictionary:
          try:
            newValue = dictionary[orgValue]
          except KeyError:
            validLine = False
            missingKeysLinesCount += 1
            missingKeys.add(orgValue)
            #print ('  ERROR in obfuscateDataFile({}{}): col = {}, value = \'{}\''.format(fileName, outExt, index, orgValue))
        else:
          newValue = orgValue  
        newLine.append(newValue)
      if validLine:  
        of.write(delimiter.join(newLine) + '\n')
        takenLines += 1
    if missingKeysLinesCount > 0:
      print ('  WARN: {} invalid lines were found, missing keys count = {}\n  {}'.format(missingKeysLinesCount, len(missingKeys), missingKeys)) 
  return totalLines, takenLines

def processDataFiles(fileName, isHierarchy = False):
  print(' processing {} ...'.format(fileName))
  #print(' processing {} ... CPU: {}% RAM: {}%'.format(fileName, psutil.cpu_percent(), psutil.virtual_memory()[2]))
  if args.orgDir:
      downloadFile(fileName, args.orgDir)
  if not os.path.exists(fileName) or os.path.getsize(fileName) == 0:
    print(' WARN: can\'t find a {} file, trying to work with .gz version'.format(fileName))
    fileNameGz = fileName.replace('.dat', '.gz')
    if args.orgDir:
      downloadFile(fileNameGz, args.orgDir)
    if not os.path.exists(fileNameGz):
      print(' ERROR: can\'t find a {} file, skipping...\n'.format(fileNameGz))
      return
    extractFile(fileNameGz, fileName)
    if args.orgDir:
      os.remove(fileNameGz)
  t1 = timer()
  try:
    if isHierarchy:
      totalLines = takenLines = obfuscateHierarchyFile(fileName, ".new")
    else:
      totalLines, takenLines = obfuscateDataFile(fileName, ".new")
  except Exception: #TODO: fix this later, we just need to go through all files for first time and catch all unhandled errors
    err = traceback.print_exc()
    errorStr = '\nERROR ' + fileName + ': ' + str(err)
    print(errorStr)
    with open(args.repFile, 'a') as rf:
      rf.write(errorStr)  
  else:
    t2 = timer()
    reportStr = ' {}: total - {} of {} lines ({:.2f} sec)\n'.format(fileName, takenLines, totalLines, t2 - t1)
    if args.verbose:
      print(reportStr)
    with open(args.repFile, 'a') as rf:
      rf.write(reportStr)
    if args.outDir:  
      uploadFile(fileName, '.new', args.outDir)
      os.remove(fileName + '.new')    
  finally:  
    if args.orgDir:
      os.remove(fileName)


def extractFile(inFile, outFile):
  with gzip.open(inFile, 'rb') as inf, open(outFile, 'wb') as outf:
    for line in inf:
      outf.write(line) 


def uploadFile(fileName, ext, filePath):
  fullPath = '{}/{}'.format(filePath, fileName)
  localFileName = '{}{}'.format(fileName, ext)
  if args.verbose:
    print('  uploading: ./{} -> {}/{}'.format(localFileName, filePath, fileName))   
  subprocess.call(["cloud-store", "upload", "-i", localFileName, "--keydir", args.keyDir, fullPath]) #use run() for python version > 3.5 


def downloadFile(fileName, filePath):
  fullPath = '{}/{}'.format(filePath, fileName)
  if args.verbose:
    print('  downloading: {}/{} -> ./{}'.format(filePath, fileName, fileName))
  subprocess.call(["cloud-store", "download", "-o", fileName, "--overwrite", "--keydir", args.keyDir, fullPath]) #use run() for python version > 3.5 


#main
print('Hierarchy files obfuscator v.0.91b')
with open(args.repFile, 'w+') as rf:
  rf.write('++++++++++++++++\nWork was started: ' + str(datetime.now()) + '\n\n')

if args.incSrc:
  inputDataFiles = inputIncomingFiles
else:
  inputDataFiles = inputBackupFiles    

#define vars
timerStart = timer()
if args.verbose:
  if args.orgDir:
    print(' work mode: remote')
  else:
    print(' work mode: local files')
  print(' hierarchy files: {}'.format(inputHierarchyFiles))
  print(' data files: {}'.format(inputDataFiles))

print('Processing:')
for file in inputHierarchyFiles:
  processDataFiles(file, isHierarchy = True)
if not args.hiersOnly:  
  for file in inputDataFiles:
    processDataFiles(file)

timerFinish = timer()
reportStr = '\nElapsed time: {:.0f} sec\n\n'.format(timerFinish - timerStart)
print(reportStr)
with open(args.repFile, 'a') as rf:
  rf.write(reportStr)
  rf.write('\nWork was finished: ' + str(datetime.now()) + '\n++++++++++++++++\n')
