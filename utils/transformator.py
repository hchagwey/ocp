#!/usr/bin/env python

from __future__ import print_function
import os
import sys
import glob
import gzip
import os.path
import argparse
import traceback
import subprocess
from datetime import datetime
from collections import namedtuple
from timeit import default_timer as timer

#transformations for derived files, reduces original value down to 20% (NB. output measure name as a key)
def reduceValueTo20pct(fieldHeader):
  #print('reduceValueTo20pct(', fieldHeader, ')')
  return {
    #you can add special cases here
    'OOClear_C_TY': lambda val: str(int(round(float(val) * 0.2))),
    'RecClear_C_TY': lambda val: str(int(round(float(val) * 0.2)))
  }.get(fieldHeader, lambda val: str(float(val) * 0.2)) #default, float 20% of the previous value

def setValueToZero(fieldHeader):
  return {
    #you can add special cases here
    'OOClear_C_TY': lambda val: '0'
  }.get(fieldHeader, lambda val: '0.0') #by default, float value  

#config section
hierarchyFiles = { #hierarchy files
  #spa_output_file, crate_input_files
  'Time.dlm' : ['00_time.dat'],
  'Product.dlm' : ['01_product.dat'],
  'Location.dlm' : ['02_location.dat']  
}

backupFiles = { #backup/restore data files
  #spa_output_file, crate_input_files 

  #these files are outdated
  
  ##Backup/Restore
  #'stage_in_BOPReg_C_TY.dlm' : ['file_bop_merch_BOPMerch_TY_C_INB.dat'],
  #'stage_in_BOPClear_C_TY.dlm' : ['file_bop_undelivered_BOPUndeliver_TY_C_INB.dat'],
  #'stage_in_DebitTtlAllowance_C_TY.dlm' : ['file_chargeback_Chargeback_TY_R_INB.dat'],
  #'stage_in_SlsCompTtl_R_TY.dlm' : ['file_sales_net_promo_SlsNetPromo_TY_R_INB.dat'], #was: 'file_comp_Comp_TY_B_INB.dat' but we haven't Sku column in Crate's input file
  #'stage_in_OOReg_C_TY.dlm' : ['file_onorder_OnordVendor_TY_C_INB.dat'],
  #'stage_in_RecReg_C_TY.dlm' : ['file_receipts_Rcpt_TY_C_INB.dat'],
  #'stage_in_RTVReg_C_TY.dlm' : ['file_rtv_RTV_TY_C_INB.dat'],
  #'stage_in_COGSClear_C_TY.dlm' : ['file_sales_dropship_SlsDropShip_TY_C_INB.dat'],
  #'stage_in_SlsNetClear_R_TY.dlm' : ['file_sales_dropship_SlsDropShip_TY_R_INB.dat'],
  #'stage_in_COGSReg_C_TY.dlm' : ['file_sales_net_promo_SlsNetPromo_TY_C_INB.dat'],
  #'stage_in_SlsNetReg_R_TY.dlm' : ['file_sales_net_promo_SlsNetPromo_TY_R_INB.dat'],
  #'stage_in_ShrinkReg_C_TY.dlm' : ['file_shrink_Shrink_TY_C_INB.dat'],
  ##former derived files
  #'stage_in_ShrinkClear_C_TY.dlm' : ['file_shrink_Shrink_TY_C_INB.dat'],
  #'stage_in_RTVClear_C_TY.dlm' : ['file_rtv_RTV_TY_C_INB.dat'],
  #'stage_in_OOClear_C_TY.dlm' : ['file_onorder_OnordVendor_TY_C_INB.dat'],
  #'stage_in_RecClear_C_TY.dlm' : ['file_receipts_Rcpt_TY_C_INB.dat'],

  #stage-import-measures
  'SkuStoreWeek_Adj.dlm' : ['file_bop_merch_BOPMerch_TY_C_INB.dat'], #HACK: temporary solution
  'SkuStoreWeek_BOP.dlm' : ['file_bop_merch_BOPMerch_TY_C_INB.dat', 'file_bop_undelivered_BOPUndeliver_TY_C_INB.dat'],
  'SkuStoreWeek_COGS.dlm' : ['file_sales_dropship_SlsDropShip_TY_C_INB.dat', 'file_sales_net_promo_SlsNetPromo_TY_C_INB.dat'],
  'SkuStoreWeek_DebitTtlAllowance.dlm' : ['file_chargeback_Chargeback_TY_R_INB.dat'],
  'SkuStoreWeek_OO.dlm' : ['file_onorder_OnordVendor_TY_C_INB.dat'],
  'SkuStoreWeek_RTV.dlm' : ['file_rtv_RTV_TY_C_INB.dat'],
  'SkuStoreWeek_Rec.dlm' : ['file_receipts_Rcpt_TY_C_INB.dat'],
  'SkuStoreWeek_Shrink.dlm' : ['file_shrink_Shrink_TY_C_INB.dat'],
  'SkuStoreWeek_SlsCompTtl.dlm' : ['file_sales_net_promo_SlsNetPromo_TY_R_INB.dat'],  #was: 'file_comp_Comp_TY_B_INB.dat' but we haven't Sku column in Crate's input file
  'SkuStoreWeek_SlsNet.dlm' : ['file_sales_net_promo_SlsNetPromo_TY_R_INB.dat', 'file_sales_dropship_SlsDropShip_TY_R_INB.dat']
}

incomingFiles = {
    #Inputs
  'SkuStoreWeek_Adj.dlm' : ['bop_merch.dat'], #HACK: temporary solution
  'SkuStoreWeek_BOP.dlm' : ['bop_undelivered.dat', 'bop_merch.dat'],
  'SkuStoreWeek_COGS.dlm' : ['sales_dropship.dat', 'sales_net.dat'], #SlsNetPromo_TY_C_INB is missing, replaced with the SlsNetTtl_TY_C_INB
  'SkuStoreWeek_DebitTtlAllowance.dlm' : ['chargeback.dat'],
  'SkuStoreWeek_OO.dlm' : ['onorder.dat'],
  'SkuStoreWeek_RTV.dlm' : ['rtv.dat'],
  'SkuStoreWeek_Rec.dlm' : ['receipts.dat'],
  'SkuStoreWeek_Shrink.dlm' : ['shrink.dat'],
  'SkuStoreWeek_SlsCompTtl.dlm' : ['sales_net.dat'],  #SlsNetPromo_TY_R_INB is missing, replaced with the SlsNetTtl_TY_R_INB was: 'file_comp_Comp_TY_B_INB.dat' but we haven't Sku column in Crate's input file
  'SkuStoreWeek_SlsNet.dlm' : ['sales_dropship.dat', 'sales_gross.dat'] #SlsNetPromo_TY_R_INB is missing, replaced with the SlsGross_TY_R_INB
}

defaultRows = {
# {0} - Week, {1} - Sku, {2} - Store
  'file_sales_net_promo_SlsNetPromo_TY_R_INB.dat' : 'R|{2}|{1}|{0}|0.0', #Type|Store|Sku|Week|SlsNetPromo_TY_R_INB
  'file_sales_dropship_SlsDropShip_TY_R_INB.dat' : '{1}|0|{2}|{0}', #Sku|SlsDropShip_TY_R_INB|Store|Week
  'file_bop_merch_BOPMerch_TY_C_INB.dat' : '0.0|{1}|{2}|C|{0}', #BOPMerch_TY_C_INB|Sku|Store|Type|Week
  'file_bop_undelivered_BOPUndeliver_TY_C_INB.dat' : '0.0|{1}|{2}|C|{0}', #BOPUndeliver_TY_C_INB|Sku|Store|Type|Week
  'file_sales_dropship_SlsDropShip_TY_C_INB.dat' : '{1}|0.0|{2}|{0}', #Sku|SlsDropShip_TY_C_INB|Store|Week
  'file_sales_net_promo_SlsNetPromo_TY_C_INB.dat' : 'C|{2}|{1}|{0}|0.0', #Type|Store|Sku|Week|SlsNetPromo_TY_C_INB
  #default order, {0}|{1}|{2} == {}|{}|{}
  'bop_undelivered.dat' : '{}|{}|{}|C|0.0|0.0|0', #Week|Sku|Store|Type|BOPUndeliver_TY_R_INB|BOPUndeliver_TY_C_INB|BOPUndeliver_TY_U_INB
  'bop_merch.dat' : '{}|{}|{}|C|0.0|0.0|0', #Week|Sku|Store|Type|BOPMerch_TY_R_INB|BOPMerch_TY_C_INB|BOPMerch_TY_U_INB
  'sales_net.dat' : '{}|{}|{}|C|0.0|0.0|0', #Week|Sku|Store|Type|SlsNetTtl_TY_R_INB|SlsNetTtl_TY_C_INB|SlsNetTtl_TY_U_INB
  'sales_gross.dat' : '{}|{}|{}|0.0|0.0|0', #Week|Sku|Store|SlsGross_TY_R_INB|SlsGross_TY_C_INB|SlsGross_TY_U_INB
  'sales_dropship.dat' : '{}|{}|{}|0.0|0.0|0' #Week|Sku|Store|SlsDropShip_TY_R_INB|SlsDropShip_TY_C_INB|SlsDropShip_TY_U_INB
}

TransfAction = namedtuple("TransfAction", "orgName, newName, orgPos, newPos, valueAction") #we'll rename field if newName !='', and/or move it to the another column in case of newPos >= 0 
TransfAction.__new__.__defaults__ = (None,) * len(TransfAction._fields)

#sets of actions for specific output files
#NB: we will apply commonActions only for data files (Backup/Restore, Input, etc.)
actionsForAllFiles = {
  #Hierarchy
  'Time.dlm' : [
    TransfAction('Month', 'Time:Month:id', -1, 2),
    TransfAction('MonthLabel', 'Time:Month:label', -1, 3),
    TransfAction('Quarter', 'Time:Quarter:id', -1, 4),
    TransfAction('QuarterLabel', 'Time:Quarter:label', -1, 5),
    TransfAction('Season', 'Time:Season:id', -1, 6),
    TransfAction('SeasonLabel', 'Time:Season:label', -1, 7),
    TransfAction('Week', 'Time:Week:id', -1, 0),
    TransfAction('WeekLabel', 'Time:Week:label', -1, 1),
    TransfAction('Year', 'Time:Year:id', -1, 8),
    TransfAction('YearLabel', 'Time:Year:label', -1, 9)
  ],
  'Product.dlm' : [
    TransfAction('Brand', 'Product:Division:id', -1, 10),
    TransfAction('BrandLabel', 'Product:Division:label', -1, 11),
    TransfAction('Class', 'Product:Class:id', -1, 4),
    TransfAction('ClassLabel', 'Product:Class:label', -1, 5),
    TransfAction('Company', 'Product:Brand:id', -1, 8),
    TransfAction('CompanyLabel', 'Product:Brand:label', -1, 9),
    TransfAction('Department', 'Product:Dept:id', -1, 6),
    TransfAction('DepartmentLabel', 'Product:Dept:label', -1, 7),
    TransfAction('Sku', 'Product:Sku:id', -1, 0),
    TransfAction('SkuLabel', 'Product:Sku:label', -1, 1),
    TransfAction('Subclass', 'Product:Subclass:id', -1, 2),
    TransfAction('SubclassLabel', 'Product:Subclass:label', -1, 3)
  ],
  'Location.dlm' : [
    TransfAction('Chain', 'Location:Company:id', -1, 4),
    TransfAction('ChainLabel', 'Location:Company:label', -1, 5),
    TransfAction('Channel', 'Location:Channel:id', -1, 2),
    TransfAction('ChannelLabel', 'Location:Channel:label', -1, 3),
    TransfAction('Store', 'Location:Store:id', -1, 0),
    TransfAction('StoreLabel', 'Location:Store:label', -1, 1)
  ],
  #Backup/Restore (see commonActions for columns 0-2
  'stage_in_BOPReg_C_TY.dlm' : [
    TransfAction('BOPMerch_TY_C_INB', 'BOPReg_C_TY', -1, 3) #BOPMerch_TY_C_INB will be renamed to BOPReg_C_TY and moved to column #3 
  ],
  'stage_in_BOPClear_C_TY.dlm' : [
    TransfAction('BOPUndeliver_TY_C_INB', 'BOPClear_C_TY', -1, 3)
  ],
  'stage_in_DebitTtlAllowance_C_TY.dlm' : [
    TransfAction('Chargeback_TY_R_INB', 'DebitTtlAllowance_C_TY', -1, 3)
  ],
  'stage_in_SlsCompTtl_R_TY.dlm' : [
    TransfAction('SlsNetPromo_TY_R_INB', 'SlsCompTtl_R_TY', -1, 3) #was: TransfAction('Comp_TY_B_INB', 'SlsCompTtl_R_TY', -1, 3)
  ],
  'stage_in_OOReg_C_TY.dlm' : [
    TransfAction('OnordVendor_TY_C_INB', 'OOReg_C_TY', -1, 3)
  ],
  'stage_in_RecReg_C_TY.dlm' : [
    TransfAction('Rcpt_TY_C_INB', 'RecReg_C_TY', -1, 3)
  ],
  'stage_in_RTVReg_C_TY.dlm' : [
    TransfAction('RTV_TY_C_INB', 'RTVReg_C_TY', -1, 3)
  ],
  'stage_in_COGSClear_C_TY.dlm' : [
    TransfAction('SlsDropShip_TY_C_INB', 'COGSClear_C_TY', -1, 3)
  ],
  'stage_in_SlsNetClear_R_TY.dlm' : [
    TransfAction('SlsDropShip_TY_R_INB', 'SlsNetClear_R_TY', -1, 3)
  ],
  'stage_in_COGSReg_C_TY.dlm' : [
    TransfAction('SlsNetPromo_TY_C_INB', 'COGSReg_C_TY', -1, 3)
  ],
  'stage_in_SlsNetReg_R_TY.dlm' : [
    TransfAction('SlsNetPromo_TY_R_INB', 'SlsNetReg_R_TY', -1, 3)
  ],
  'stage_in_ShrinkReg_C_TY.dlm' : [
    TransfAction('Shrink_TY_C_INB', 'ShrinkReg_C_TY', -1, 3)
  ],
  #former Backup/restore derived files
  'stage_in_ShrinkClear_C_TY.dlm' : [
    TransfAction('Shrink_TY_C_INB', 'ShrinkClear_C_TY', -1, 3, reduceValueTo20pct)
  ],
  'stage_in_RTVClear_C_TY.dlm' : [
    TransfAction('RTV_TY_C_INB', 'RTVClear_C_TY', -1, 3, reduceValueTo20pct)
  ],
  'stage_in_OOClear_C_TY.dlm' : [
    TransfAction('OnordVendor_TY_C_INB', 'OOClear_C_TY', -1, 3, reduceValueTo20pct)
  ],
  'stage_in_RecClear_C_TY.dlm' : [
    TransfAction('Rcpt_TY_C_INB', 'RecClear_C_TY', -1, 3, reduceValueTo20pct)
  ],
  #stage-import-measures
  'SkuStoreWeek_Adj.dlm' : [
    TransfAction('BOPMerch_TY_C_INB', 'AdjClear_C_TY', -1, 3, setValueToZero),
    TransfAction('BOPMerch_TY_C_INB', 'AdjReg_C_TY', -1, 4, setValueToZero)
  ],
  'SkuStoreWeek_BOP.dlm' : [
    TransfAction('BOPUndeliver_TY_C_INB', 'BOPClear_C_TY', -1, 3),
    TransfAction('BOPMerch_TY_C_INB', 'BOPReg_C_TY', -1, 4)
  ],
  'SkuStoreWeek_COGS.dlm' : [
    TransfAction('SlsDropShip_TY_C_INB', 'COGSClear_C_TY', -1, 3),
    TransfAction('SlsNetPromo_TY_C_INB', 'COGSReg_C_TY', -1, 4)
  ],
  'SkuStoreWeek_DebitTtlAllowance.dlm' : [
    TransfAction('Chargeback_TY_R_INB', 'DebitTtlAllowance_C_TY', -1, 3)
  ],
  'SkuStoreWeek_OO.dlm' : [
    TransfAction('OnordVendor_TY_C_INB', 'OOClear_C_TY', -1, 3, reduceValueTo20pct),
    TransfAction('OnordVendor_TY_C_INB', 'OOReg_C_TY', -1, 4)
  ],
  'SkuStoreWeek_RTV.dlm' : [
    TransfAction('RTV_TY_C_INB', 'RTVClear_C_TY', -1, 3, reduceValueTo20pct),
    TransfAction('RTV_TY_C_INB', 'RTVReg_C_TY', -1, 4)
  ],
  'SkuStoreWeek_Rec.dlm' : [
    TransfAction('Rcpt_TY_C_INB', 'RecClear_C_TY', -1, 3, reduceValueTo20pct),
    TransfAction('Rcpt_TY_C_INB', 'RecReg_C_TY', -1, 4)
  ],
  'SkuStoreWeek_Shrink.dlm' : [
    TransfAction('Shrink_TY_C_INB', 'ShrinkClear_C_TY', -1, 3, reduceValueTo20pct),
    TransfAction('Shrink_TY_C_INB', 'ShrinkReg_C_TY', -1, 4)
  ],
  'SkuStoreWeek_SlsCompTtl.dlm' : [
    TransfAction('SlsNetPromo_TY_R_INB', 'SlsCompTtl_R_TY', -1, 3) #was: TransfAction('Comp_TY_B_INB', 'SlsCompTtl_R_TY', -1, 3)
  ],
  'SkuStoreWeek_SlsNet.dlm' : [
    TransfAction('SlsDropShip_TY_R_INB', 'SlsNetClear_R_TY', -1, 3),
    TransfAction('SlsNetPromo_TY_R_INB', 'SlsNetReg_R_TY', -1, 4)
  ]
}

actionsForIncomingFiles = {
  #stage-import-measures partially based on DIFFERENT Crate's measures
  'SkuStoreWeek_COGS.dlm' : [
    TransfAction('SlsDropShip_TY_C_INB', 'COGSClear_C_TY', -1, 3),
    TransfAction('SlsNetTtl_TY_C_INB', 'COGSReg_C_TY', -1, 4) #was: SlsNetPromo_TY_C_INB
  ],
  'SkuStoreWeek_SlsCompTtl.dlm' : [
    TransfAction('SlsNetTtl_TY_R_INB', 'SlsCompTtl_R_TY', -1, 3) #was: SlsNetPromo_TY_R_INB, TransfAction('Comp_TY_B_INB', 'SlsCompTtl_R_TY', -1, 3)
  ],
  'SkuStoreWeek_SlsNet.dlm' : [
    TransfAction('SlsDropShip_TY_R_INB', 'SlsNetClear_R_TY', -1, 3),
    TransfAction('SlsGross_TY_R_INB', 'SlsNetReg_R_TY', -1, 4) #was: SlsNetPromo_TY_R_INB
  ]
}


#additional actions which were applied to all files
commonActions = [
    #Ex: TransfAction("Sku_Code", "SKU", -1, 0) #Column 'Sku_Code' will me renamed to the 'SKU' and moved to the very first column from anywere
    TransfAction('Sku', 'Product:Sku:id', -1, 0),
    TransfAction('Store', 'Location:Store:id', -1, 1),
    TransfAction('Week', 'Time:Week:id', -1, 2)
]

#functions for formatTimeValue
def makeWeekId(val):
  date = datetime.strptime(val, "%Y%m%d")
  isodate = date.isocalendar()
  makeWeekId.year = str(isodate[0])
  return makeWeekId.year + '_Wk_' + '{:02d}'.format(isodate[1])

def makeSpaId(val, mid):
  num, year = val[:2], val[2:]
  return year + mid + num

def makeSeason(val):
  season = {
    'SP' : '01',
    'FA' : '02'
  }
  s, y = val[:2], val[2:]
  return y + '_S_' + season[s]

#special calendar modifications (input column name as a key)
def formatTimeValue(fieldHeader):
  return {
    'Week': lambda val: makeWeekId(val),
    'WeekLabel': lambda val: val + ' ' + makeWeekId.year,
    'Month': lambda val: makeSpaId(val, '_M_'),
    'MonthLabel': lambda val: val.replace(' ', ' 20'),
    'Quarter': lambda val: makeSpaId(val, '_Q_'),
    'QuarterLabel': lambda val: val.replace(' ', ' 20'),
    'Season': lambda val: makeSeason(val),
    'Year': lambda val: val + '_Y'    
  }.get(fieldHeader, lambda val: val)

#sample usage: python transformator.py
parser = argparse.ArgumentParser(description = '')
parser.add_argument('-org', '--orgDir', dest='orgDir', help="subdir's name with original Data set", default='')
parser.add_argument('-out', '--outDir', dest='outDir', help="subdir's name with obfuscated Data set", default='')
parser.add_argument('-kd', '--keyDir', dest='keyDir', help="param for cloud-store", default='/run/keys/')
parser.add_argument('-gz', '--gZip', action='store_true', dest='gZip', help="pack output files ", default = False)
#there are 2 modes: use backup/restore or incoming files as source data tor transformation, by default we use backup/restore files 
parser.add_argument('-incs', '--incSrc', action='store_true', dest='incSrc', help="", default = False)
parser.add_argument('-hiers', '--hiersOnly', action='store_true', dest='hiersOnly', help="", default = False)
parser.add_argument('-r', '--report', dest='repFile', help="name of report file with summary information", default='transformator.log')
parser.add_argument('-v', '--verbose', action='store_true', dest='verbose', help='print more status messages to stdout', default = False)
parser.add_argument('-ne', '--useNewExt', action='store_true', dest='useNewExt', help='', default = False)
args = parser.parse_args()


def combineData(orgData, inFileNames):
  headerLine = ''
  inputsCnt = len(orgData)
  if inputsCnt > 2:
    print('ERROR: for now, we can work only with 2 inputs! Please, modify #TODO: line first')
    inputsCnt = 2
  valuesDicts = [None] * inputsCnt # list of dicts {SkuStoreWeek : originalRow}

  #create dict
  for inp in range(0, inputsCnt):
    valuesDicts[inp] = {}
    #save 1st line
    hdr = orgData[inp].pop(0).strip('\n')
    if headerLine:
      headerLine = headerLine + '|' + hdr
    else:
      headerLine = hdr   
    
    #parse header
    headerFieldsList = hdr.split("|")
    skuInd = headerFieldsList.index('Sku')
    storeInd = headerFieldsList.index('Store')
    weekInd = headerFieldsList.index('Week')
    if args.verbose:
      print('header: {} inds: {} {} {}'.format(hdr, skuInd, storeInd, weekInd))
    for line in orgData[inp]:
      line = line.strip('\n')
      lineList = line.split("|")
      key = '{}|{}|{}'.format(lineList[skuInd], lineList[storeInd], lineList[weekInd])
      #value = valuesDicts[inp].get(key, None)
      #if value:
      #  print('warning, key already exists: ', key, ' = ', value, ' -> ', line)
      valuesDicts[inp][key] = line
    if args.verbose:
      print('dict size: ', len(valuesDicts[inp]))
    del orgData[inp][:] #python3: orgData[inp].clear()          

  #generate combined data
  outputList = [[headerLine]]
  for key in valuesDicts[0].keys() + valuesDicts[1].keys():
    #TODO: we need for-cycle here
    vals = [None, None]
    for n in [0, 1]:
      vals[n] = valuesDicts[n].get(key, None)
      if not vals[n]:
        frmStr = defaultRows.get(inFileNames[n], None)
        if frmStr:
          skuVal, storeVal, weekVal = key.split('|') #sku, store, week
          vals[n] = frmStr.format(weekVal, skuVal, storeVal) #see defaultRows dictionary 
        else:
          vals[n] = '0.0' #HACK: in all other cases we have a float values there 
    outputList[0].append(vals[0] + '|' + vals[1])
  if args.verbose and len(outputList[0]) > 3:
    print('list size: ', len(outputList[0]))
    for row in range(0, 3):
      print('  INFO: Combined (raw) row: {}'.format(outputList[0][row].strip('\n')))

  return outputList    


weekDict = {}
def transformFile(inFileNames, outFileName, actions):
  delimiter = '|'
  totalLines = 0
  skippedLines = 0
  sswItems = set()
  inputsCnt = len(inFileNames)
  orgData = [None] * inputsCnt #we can use '' instead of None to avoid errors in case of missing TransfActions for some required columns
  newHeaderList = [None] * len(actions) #was: headerFieldsList
  timeHierarchyCase = outFileName == 'Time.dlm' or outFileName == 'Time.dlm.gz'
  sswCase = 'SkuStoreWeek_' in outFileName
  
  ind = 0
  for inFileName in inFileNames:
    with open(inFileName) as orgf:
      orgData[ind]=orgf.readlines()
      ind += 1

  #combine fields
  if inputsCnt > 1:
    orgData = combineData(orgData, inFileNames)
 
  #get header's fields
  weekColIndex = -1
  headerFieldsList = orgData[0].pop(0).strip('\n').split("|")
  for pos, field in enumerate(headerFieldsList):
    for apos, action in enumerate(actions):
      if weekColIndex < 0 and field == 'Week':
        weekColIndex = pos
      if field == action.orgName:
        if action.orgPos < 0:
          actions[apos] = action._replace(orgPos=pos)
        if action.newName:
          newFieldName = action.newName
        else:
          newFieldName = field
        if action.newPos >= 0:
          newHeaderList[action.newPos] = newFieldName
        else:
          newHeaderList[pos] = newFieldName
        #break #now, we can use the same value for several actions

  #process all rows
  if args.gZip:
    fileStream = gzip.open
  else:
    fileStream = open
  with fileStream(outFileName, 'wb') as of:   
    of.write(delimiter.join(newHeaderList) + '\n')
    for line in orgData[0]:
      orgLineList = line.strip('\n').split("|")
      newLine = [None] * len(actions) #was: orgLineList
      for pos, orgValue in enumerate(orgLineList):
        if timeHierarchyCase:
          newValue = formatTimeValue(headerFieldsList[pos])(orgValue)
          if pos == weekColIndex: #we need a 'Week' dictionary for backup/restore data files
            if weekDict.get(orgValue, None):
              print('ERROR: duplicate Week Id (', orgValue, ') was found in the original file.') 
            weekDict[orgValue] = newValue
          orgValue = newValue
        else:
          if weekColIndex >= 0 and pos == weekColIndex: #we have a 'Week' column here and it's not a time hierarchy file
            orgValue = weekDict[orgValue]
        for action in actions:
          #we need to reorder values here
          if pos == action.orgPos:
            if action.valueAction:
              #print('valueAction: ', action.newName, ', value: ', orgValue)
              newValue = action.valueAction(action.newName)(orgValue)
            else:
              newValue = orgValue  
            if action.newPos >= 0:
              newLine[action.newPos] = newValue
            else:
              newLine[pos] = newValue
            #break #now, we can use the same value for several actions
      if None in newLine:
        print('WARN: in ', outFileName, ' an empty output column was found ', orgLineList, '->', newLine) 
      
      addLine = True
      if sswCase:
        sswKey = delimiter.join(newLine[:3])
        if sswKey in sswItems:
          addLine = False
          skippedLines += 1
          #if args.verbose:
          #  print('WARN: in ', outFileName, ' a duplicate line was found ', sswKey) 
        else:  
          sswItems.add(sswKey)

      if addLine:
        of.write(delimiter.join(newLine) + '\n')
      totalLines += 1

  if timeHierarchyCase:
    #additional check for duplicates
    revDict={}
    for key, value in weekDict.items(): 
      revDict.setdefault(value, set()).add(key) 
    dupDict = [key for key, values in revDict.items() if len(values) > 1] 
    # printing result
    if len(dupDict): 
      print("WARN: duplicate values ", dupDict)
    if args.verbose:
      print(' weekColIndex = ', weekColIndex, ', weekDict size = ', len(weekDict))
    #if args.verbose:
    #  print(inFileName + ' RAM END: ' + psutil.virtual_memory())  #physical memory usage
    
  return totalLines, skippedLines


def processDataFiles(inFileNames, outFileName, actionsForFiles, addCommonActions = False):
  #setup actions
  actions = actionsForFiles.get(outFileName, [])
  if addCommonActions and commonActions:
    actions += commonActions

  if args.gZip:
    outFileName += '.gz'
  print(' processing {} to {}...'.format(inFileNames, outFileName))
  #print(' processing {} ... CPU: {}% RAM: {}%'.format(outFileName, psutil.cpu_percent(), psutil.virtual_memory()[2]))
  if args.orgDir:
    for inFileName in inFileNames:
      downloadFile(inFileName, args.orgDir)
  
  for inFileName in inFileNames:
    #cloud-store creates an empty files in case of missing source file
    if not os.path.isfile(inFileName) or not os.path.getsize(inFileName):
      print('ERROR: ', inFileName, ' is missing, skipping...')
      return

  t1 = timer()
  try:
    totalLines, skippedLines = transformFile(inFileNames, outFileName, actions)  
  except Exception: #TODO: fix this later, we just need to go through all files for first time and catch all unhandled errors
    err = traceback.print_exc()
    errorStr = '\nERROR ' + outFileName + ': ' + str(err)
    print(errorStr)
    with open(args.repFile, 'a') as rf:
      rf.write(errorStr)  
  else:
    t2 = timer()
    reportStr = ' {}: total - {} lines, {} skipped ({:.1f} sec)\n'.format(outFileName, totalLines, skippedLines, t2 - t1)
    if args.verbose:
      print(reportStr)
    with open(args.repFile, 'a') as rf:
      rf.write(reportStr)
    if args.outDir:  
      uploadFile(outFileName, args.outDir)
      os.remove(outFileName)    
  finally:  
    if args.orgDir:
      for inFileName in inFileNames:
        os.remove(inFileName)


def uploadFile(fileName, filePath):
  fullPath = '{}/{}'.format(filePath, fileName)
  if args.verbose:
    print('  uploading: ./{} -> {}'.format(fileName, fullPath))   
  subprocess.call(["cloud-store", "upload", "-i", fileName, "--keydir", args.keyDir, fullPath]) #use run() for python version > 3.5 


def downloadFile(fileName, filePath):
  fullPath = '{}/{}'.format(filePath, fileName)
  if args.verbose:
    print('  downloading: {}/{} -> ./{}'.format(filePath, fileName, fileName))
  subprocess.call(["cloud-store", "download", "-o", fileName, "--overwrite", "--keydir", args.keyDir, fullPath]) #use run() for python version > 3.5 


#main
print('Backup/Recovery data transformator v.0.9b')
with open(args.repFile, 'a+') as rf:
  rf.write('++++++++++++++++\nWork was started: ' + str(datetime.now()) + '\n\n')

#define vars
timerStart = timer()
print('Processing hierarchy files:')
if args.verbose:
  print(' list of files: {}'.format(hierarchyFiles))
for outFile, inFiles in hierarchyFiles.iteritems(): #.items() for Python 3.x
  if args.useNewExt:
    inFiles = ['{}.new'.format(fname) for fname in inFiles] #when we use obfuscator script locally, it leaves obfuscated files with d .new file extension
  processDataFiles(inFiles, outFile, actionsForAllFiles)

if args.hiersOnly:
  sys.exit()  

actionsForFiles = actionsForAllFiles 
if args.incSrc:
  dataFiles = incomingFiles
  actionsForFiles.update(actionsForIncomingFiles)
else:
  dataFiles = backupFiles  
print('Processing data files:')
if args.verbose:
  print(' list of files: {}'.format(dataFiles))
for outFile, inFiles in dataFiles.iteritems():
  if args.useNewExt:
    inFiles = ['{}.new'.format(fname) for fname in inFiles] #when we use obfuscator script locally, it leaves obfuscated files with d .new file extension
  processDataFiles(inFiles, outFile, actionsForFiles, addCommonActions = True)

timerFinish = timer()
reportStr = '\nElapsed time: {:.0f} sec\n\n'.format(timerFinish - timerStart)
print(reportStr)
with open(args.repFile, 'a') as rf:
  rf.write(reportStr)
  rf.write('\nWork was finished: ' + str(datetime.now()) + '\n++++++++++++++++\n')
