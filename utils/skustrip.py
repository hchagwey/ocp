#!/usr/bin/env python

from __future__ import print_function
import os
import glob
import gzip
import zipfile
import hashlib
import shutil
import argparse
from timeit import default_timer as timer
try:
    import zlib
    compression = zipfile.ZIP_DEFLATED
except (ImportError, AttributeError):
    compression = zipfile.ZIP_STORED

#sample usage: python skustrip.py -n 1000
parser = argparse.ArgumentParser(description = 'SPA Data Stripper aka sku Stripper v.0.1b')
parser.add_argument('-d', '--dataDir', dest='dataDir', help="subdir's name with original Data set", default='./')
parser.add_argument('-o', '--outDir', dest='outDir', help="subdir's name with stripped Data set", default='stripped/')
parser.add_argument('-v', '--verbose', action='store_true', dest='verbose', help='print more status messages to stdout', default = False)
parser.add_argument('-n', '--skuCount', dest='skuCount', help="count of SKU's")
args = parser.parse_args()

#utils
blocksize = 1 << 16     #64kB

def extractGzFile(zipName, inPath, outPath):
    fileName = os.path.splitext(zipName)[0]
    with gzip.open(inPath + zipName, 'rb') as zipf, open(outPath + fileName, 'wb+') as of:
        while True:
            block = zipf.read(blocksize)
            if block == '':
                break
            of.write(block)
        return fileName
    return None

def packGzFile(fileName, zipName, path):
    odir = os.getcwd()
    os.chdir(path)
    with open(fileName, 'rb') as inf, gzip.open(zipName, 'wb') as zipf:
        shutil.copyfileobj(inf, zipf)
    os.remove(fileName)
    os.chdir(odir)

def getFilesList(path, pattern):
    files = []
    for fileName in sorted(glob.glob(path + pattern)): #, reverse=True, key=os.path.getctime (or os.path.getmtime)
        files.append(os.path.basename(fileName))
    return files

def getSkuList(fileName, inPath, skuCount):
    skus = list() #python 3.x subClasses.copy() #was:[]
    with open(inPath + fileName) as f:
        for line in f:
            fields = line.split("|")
            skus.append(fields[0])
            if len(skus) > skuCount:
                break
    return skus[1:] #skip header

def stripFile(fileName, inPath, outPath, skus):
    totalLines = 0
    takenLines = 0
    with open(inPath + fileName) as f, open(outPath + fileName, 'w+') as of:
        for line in f:
            totalLines += 1
            if totalLines == 1:
                of.write(line)
            else:
                fields = line.split("|")
                if fields[0] in skus and '2017_Wk_52' not in line:
                    of.write(line)
                    takenLines += 1
    return [totalLines, takenLines]

def processGzDataFile(fileName, inPath, outPath, tmpPath, skus):
    timeStart = timer()
    name = extractGzFile(fileName, inPath, tmpPath)
    if not name:
        print("error: can't find file in " + fileName)
        exit()
    lines = stripFile(name, tmpPath, outPath, skus)
    time = timer() - timeStart
    if not lines[1]:
        os.remove(tmpPath + name) #unmodified file (we'll copy original .gz later)
        os.remove(outPath + name) #empty file
        return [lines, time]
    packGzFile(name, fileName, outPath)
    os.remove(tmpPath + name)
    time = timer() - timeStart #update timer

    return [fileName, lines, time]

def processSkusList(dataPath, skuCount):
    print('get SKUs:')
    name = extractGzFile('Product.dlm.gz', dataPath, tmpPath)
    if not name:
        print("error: can't find products file")
        exit()
    skus = frozenset(getSkuList(name, tmpPath, skuCount))
    os.remove(tmpPath + name)
    print(' total: {}'.format(len(skus)))

    return skus

def processGzFiles(dataPath, outPath, tmpPath, skus, verbose):
    zipFiles = getFilesList(dataPath, '*.dlm.gz');
    formatStr = ' {:<46} {:>7.0f} to {:>6.0f} lines   {:.1f} sec'
    print("\nprocessing gz files from '{}':".format(dataPath))
    for zipf in zipFiles:
        dataInfo = processGzDataFile(zipf, dataPath, outPath, tmpPath, skus)
        if len(dataInfo) > 2: #file was modified
            if args.verbose:
                print(formatStr.format(dataInfo[0], dataInfo[1][0], dataInfo[1][1], dataInfo[2]))
        else:
            if args.verbose:
                print(formatStr.format(zipf, dataInfo[0][0], dataInfo[0][0], dataInfo[1])) #same value twice = copied
            shutil.copyfile(dataPath + zipf, outPath + zipf)
        if not args.verbose:
            print('.', sep='', end='', flush=True)

#main

#define vars
timerStart = timer()

#prepare dirs
tmpPath = 'tmp/'
if not os.path.exists(tmpPath):
    os.makedirs(tmpPath)
outPath = args.outDir
if not os.path.exists(outPath):
    os.makedirs(outPath)

#first, check products
skus = processSkusList(args.dataDir, int(args.skuCount))
#process all files (including products) with selected SKUs
processGzFiles(args.dataDir, args.outDir, tmpPath, skus, args.verbose)

#cleanup
os.rmdir(tmpPath)

timerFinish = timer()
print('\nelapsed time: {:.0f} sec'.format(timerFinish-timerStart))
