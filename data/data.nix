{ fetchs3 }:
{
  # to access the logicblox-private bucket, use hologram.
  # For more info see https://logicblox.atlassian.net/wiki/display/LOG/Platform+Developer+Resources

  # To download a dataset:
  # $ hologram me
  # $ cloud-store download s3://logicblox-private/platform.predictix.com/SPA/processed_data_stage.tar.gz -o . --credential-providers-s3 ec2-metadata-service --progress

  # to upload new dataset:
  # - use `shasum -a 256 processed_data_stage.tar.gz` to get the shasum, then place it below
  # - upload the file:
  # $ hologram me
  # $ cloud-store upload -i processed_data_stage.tar.gz s3://logicblox-private/platform.predictix.com/SPA/ --credential-providers-s3 ec2-metadata-service --progress

  # **********************************************************
  # ! Data set should be located in the root processed_data folder !
  # **********************************************************

    data =
    fetchs3 {
        url = s3://logicblox-private/platform.predictix.com/SPA/processed_data_stage_0915.tar.gz;
        sha256 = "6c6e465324432423ee9c96de52da86fbcb05d5c19f0d9ebdbe5850adce72b623";
    };

    stage_data =
    fetchs3 {
        url = s3://logicblox-private/platform.predictix.com/SPA/processed_data_stage_20200521.tar.gz;
        sha256 = "81dc15f5afe9f51deaa8a62857205aa95b87118a03475a5bf5340714e9d0b01d";
    };

    old_stage_data =
    fetchs3 {
        url = s3://logicblox-private/platform.predictix.com/SPA/processed_data_stage.tar.gz;
        sha256 = "da20717c15e75527a4757cc7ad174e486477cc8c44f65960ce80d461bc43a40e";
    };
}