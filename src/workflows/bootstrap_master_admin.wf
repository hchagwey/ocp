import "$(GBM_HOME)/workflow/gbm.wf"
import "./lib/master.wf"

workflow bootstrap_master_admin(
  app_workspace_name,
  first_week,
  key,
  template_names*,
  workbook_tag,
  daily_commit_file,
  workspaces
) []
{
  lb.Log(msg="START BOOTSTRAP_MASTER_ADMIN")
  ;
  gbm.planning.DeployWorkspacePartitioned(
    $app_workspace_name,
    $workspaces,
    scope="admin",
    status="pending"
  )
  ;
  bootstrap_master_admin.common(
    $first_week,
    $key,
    $daily_commit_file,
    $template_names,
    $workbook_tag
  )
  ;
  lb.Log(msg="COMPLETE BOOTSTRAP_MASTER_ADMIN")
}

workflow bootstrap_master_admin.common(
  first_week,
  key,
  daily_commit_file,
  template_names*,
  workbook_tag,
  scope="admin"
) [commit_tuples*]
{
  lb.Log(msg="START block execution for First_Week in admin.")
  ;
  gbm.planning.ExecutleLogiQLQueryMasterPartitioned(
    query="^util:time:Current_Week[] = \"$(first_week)\".",
    $scope,
    status="pending"
  )
  ;
  lb.Log(msg="COMPLETE block execution for First_Week in admin.")
  ;
  lib.master.import_master_admin($key, $scope)
  ;
  lb.Log(msg="START block execution for grant_bootstrap_user_permissions in admin.")
  ;
  gbm.planning.ExecutleLogiQLBlocksMasterPartitioned(
    blocks={"batch_actions:grant_bootstrap_user_permissions"},
    $scope,
    status="pending"
  )
  ;
  lb.Log(msg="COMPLETE block execution for grant_bootstrap_user_permissions in admin.")
  ;
  lb.Log(msg="START block execution for initialize_allow_op_rp in admin.")
  ;
  gbm.planning.ExecutleLogiQLBlocksMasterPartitioned(
    blocks={"batch_actions:initialize_allow_op_rp"},
    $scope,
    status="pending"
  )
  ;
  lb.Log(msg="START block execution for Current_Week in admin.")
  ;
  gbm.planning.ExecutleLogiQLBlocksMasterPartitioned(
    blocks={"batch_actions:set_current_week"},
    $scope,
    status="pending"
  )
  ;
  lb.Log(msg="COMPLETE block execution for Current_Week in admin.")
  ;
  gbm.planning.CreateWorkbooksPartitioned(
    $template_names,
    $scope,
    $workbook_tag,
    max_parallel=2,
    status="pending"
    
  )
  ;
  ($commit_tuples = "tuples") <~ lb.ReadCsv(
    file=$daily_commit_file,
    delim="|",
    unquote=true)
  ;
  // Because we can't add blocks to master right now we need to build workbooks so that certain measures get populated and then commit and export. 
  // This is an ugly workaround but it's needed to be able to bootstrap the env not waiting for users to send us permission files. 
  forall(commit_tuple in $commit_tuples) [ commit_name, template_name ] {
    ($commit_name="COMMIT_NAME", $template_name="TEMPLATE_NAME") <~ lb.ParseJsonObject(json=$commit_tuple)
    ;
    gbm.planning.CommitWorkbooksPartitionedByTemplate(
      $commit_name,
      $template_name,
      $scope,
      timeout="15m",
      status="pending"
    )
  }
  ;
  lib.admin.export_admin_and_security($key, $scope)
}
