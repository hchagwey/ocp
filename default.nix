{ app_dev_tools         ? ( builtins.fetchGit { url = "ssh://git@bitbucket.org/logicblox/app-dev-tools"; } ).outPath
, config                ? ( builtins.fetchGit { url = "ssh://git@bitbucket.org/logicblox/builder-config"; } ).outPath
, gbm_src               ? ( builtins.fetchGit { url = "ssh://git@bitbucket.org/logicblox/global-batch-manager"; } ).outPath
, lamias_servicetester  ? ( builtins.fetchGit { url = "ssh://git@bitbucket.org/Predictix/lamias-servicetester"; } ).outPath
, measure_query_gen     ? ( builtins.fetchGit { url = "ssh://git@bitbucket.org/logicblox/measure-query-gen"; } ).outPath
, src_modeler_app       ? builtins.fetchGit ./.
, doCheck               ? false
, installFlags          ? "--hydra"
, nixpkgs               ? <nixpkgs>
, pkgs                  ? import <nixpkgs> {}
# Still need to figure out a way for platform_release to point to integration.
, platform_release      ? "4.28.0"
, system                ? builtins.currentSystem
}:

let
  builder_config = import config { inherit nixpkgs system; };
  # data = import ./data/data.nix { inherit (builder_config) fetchs3; };
  deploy = if pkgs.stdenv.isDarwin then false else true;
  gbm = (import gbm_src { inherit platform_release; }).build;
  includeInstaller = if pkgs.stdenv.isDarwin then false else true;
  lamias_build = (import lamias_servicetester  { inherit nixpkgs; }).build;
  mercurial = pkgs.mercurial;

  # env configuration for nix-shell and proper lb config usage  
  custom_env = ''
    # for nix-shell env
    export MEASURE_QUERY_GEN_HOME=${measure_query_gen};
    export MEASURE_QUERY_GEN_VIEWS=${src_modeler_app}/tests/lamias/relax-config.js;
    export PATH=${lamias_build}/lib/node_modules/lamias/bin:${src_modeler_app}/scripts:$PATH;
    export LAMIAS_SERVICETESTER=${lamias_build}/lib/node_modules/lamias
  '';

in
  builder_config.genericAppJobset {
    inherit installFlags includeInstaller deploy;
    logicblox = builder_config.getLB platform_release;
    build =
      builder_config.buildModelerApp {
        name = "SPA";
        nodePackages = ./node-packages.nix;
        src = src_modeler_app;
        inherit doCheck gbm;
        buildInputs = [ lamias_build pkgs.nginx pkgs.python35Packages.yapf ];
        configureFlags = [ "--with-gbm=${gbm}" ];
        GBM_HOME="${gbm}";
        APP_HOME="./.";
        APP_DEV_TOOLS="${app_dev_tools}";
        MODELER_REQUEST_TIMEOUT=3600000;
        preConfigure = custom_env;
        shellHook = custom_env;
        patchPhase = ''
          export MEASURE_QUERY_GEN_HOME=${measure_query_gen};
          export MEASURE_QUERY_GEN_VIEWS=${src_modeler_app}/tests/lamias/relax-config.js;
          export PATH=${lamias_build}/lib/node_modules/lamias/bin:$PATH;
          export LAMIAS_SERVICETESTER=${lamias_build}/lib/node_modules/lamias
        '';

        postInstall = ''
          mkdir -p $out/nix-support
          for doc in $out/share/doc/*html $out/share/doc/workflows
          do
            echo "doc $(basename $doc | sed 's/\./-/g') $doc" >> $out/nix-support/hydra-build-products
          done
          for report in $out/share/doc/*txt $out/share/doc/IDE_reports/*csv
          do
            echo "report $(basename $report | sed 's/\./-/g') $report" >> $out/nix-support/hydra-build-products
          done
        '';
      };

      extraNixPath = {
        inherit gbm lamias_build measure_query_gen;
      };
  }
