#!/usr/bin/env bash

set -ex

#############################################################################################
# GBM import batches which need files copied
#############################################################################################

INITIAL_IMPORTS=""

INITIAL_VARIABLE_IMPORTS=""

DAILY_IMPORTS="stage-import-time-hierarchy \
               stage-import-product-hierarchy \
               stage-import-location-hierarchy"

DAILY_VARIABLE_IMPORTS="workflow:config:stage_batch_tag_import"

#############################################################################################

SOURCE=""
DESTINATION=""
DATE=""
TYPE=daily
MISSING_FILES=""
MISSING_BATCHES=""

#############################################################################################

usage()
{
  echo "usage: $0 -s|--source s3://location-of-source-test-data -d|--dest s3://location-to-copy-data-to [-t|--type initial|daily] -c|--current_day YYYYMMDD [-h|--help print this message]"
}

for i in "$@"
do
    case $i in
        -s=*|--source=*)
            SOURCE="${i#*=}"
            shift 
            ;;

        -d=*|--destination=*)
            DESTINATION="${i#*=}"
            shift 
            ;;

        -t=*|--type=*)
            TYPE="${i#*=}"
            shift 
            ;;

        -c=*|--current_day=*)
            DATE="${i#*=}"
            shift 
            ;;

        -h|--help)
            usage
            exit 0
            ;;
        *)
            # unknown option
            usage
            exit 1
            ;;
    esac
done

if [[ "$SOURCE" == "" ]]
then
    echo
    echo "ERROR: -s|--source parameter must be specified"
    echo 
    usage
    exit -1
fi

if [[ "$DESTINATION" == "" ]]
then
    echo
    echo "ERROR: -d|--destination parameter must be specified"
    echo 
    usage
    exit -1
fi

if [[ "$TYPE" != "initial" && "$TYPE" != "daily" ]]
then
    echo
    echo "ERROR: invalid type - $TYPE"
    echo
    usage
    exit -1
fi

if [[ "$DATE" == "" ]]
then
    echo
    echo "ERROR: -dt|--date parameter must be specified"
    echo 
    usage
    exit -1
fi

copy_batch_files()
{
    batch_name=$1
    echo
    echo "Processing $batch_name"
    json_file_count=$(grep -ilw \"$batch_name\" $APP_HOME/config/*.json $SPA_HOME/config/*.json | wc -l)
    if [ $json_file_count -ne 1 ]
    then 
        MISSING_BATCHES="${MISSING_BATCHES} ${batch_name}"
    else
        json_file=$(grep -ilw \"$batch_name\" $APP_HOME/config/*.json $SPA_HOME/config/*.json)
        files=$(jq '.[] | select(.batch_name=="'$batch_name'") .transactions[] .tdx_inputs[] | "\(.data_dir)\(.file)"' $json_file | sed 's/"//g')
        for file in $files
        do
            echo "copying $SOURCE/$DATE/$file to $DESTINATION/$DATE/$file"
            cloud-store copy $SOURCE/$DATE/$file $DESTINATION/$DATE/$file
            if [[ $? -ne 0 ]]
            then
                MISSING_FILES="${MISSING_FILES} ${batch_name}-${SOURCE}/$DATE/$file"
            fi
        done
    fi
}

#############################################################################################
# Copy Initial Data
#############################################################################################

if [[ "$TYPE" == "initial" ]]
then
    # Copy config files needed for bootstrap

    echo "copying $SOURCE/config/ to $DESTINATION/config/"
    cloud-store copy --recursive $SOURCE/config/ $DESTINATION/config/

    if [[ $? -ne 0 ]]
    then
        echo "ERROR: config files could not be copied from $SOURCE/config"
        exit -1
    fi

    for import in $INITIAL_IMPORTS
    do
        copy_batch_files $import
    done
fi

#############################################################################################
# Copy Daily Data
#     Note: Daily imports are needed for both initial and daily
#############################################################################################

for import in $DAILY_IMPORTS
do
    copy_batch_files $import
done

for var in $DAILY_VARIABLE_IMPORTS
do
    echo
    echo "Processing variable $var"
    import=$(lb query /workflow '_(result) <- '$var'[] = result.' --raw | grep -v "\-\-\-")
    copy_batch_files $import
done

#############################################################################################
# Report any missing batches or files
#############################################################################################

error=0

if [[ "$MISSING_BATCHES" != "" ]]
then
    echo
    echo "ERROR: The following batches could not be found in any configuration files under $SPA_HOME/config/ or $APP_HOME/config/"
    for batch in $MISSING_BATCHES
    do
        echo $batch
    done
    error=-1
fi

if [[ "$MISSING_FILES" != "" ]]
then
    echo
    echo "ERROR: The following files could not be copied"
    for file in $MISSING_FILES
    do
        echo $file
    done
    error=-1
fi

exit $error
