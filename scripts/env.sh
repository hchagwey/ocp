#!/usr/bin/env bash

# LB_COMPONENTS
source "upstream/logicblox/etc/profile.d/logicblox.sh"
source "upstream/logicblox/etc/bash_completion.d/logicblox.sh"

# Required for workspace creation in make deploy
export  LB_CONNECTBLOX_ENABLE_ADMIN=1

# Set up GBM
export APP_HOME=$PWD
export GBM_HOME=$APP_HOME/upstream/gbm
export SPA_HOME=$APP_HOME
