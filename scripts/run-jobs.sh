#! /usr/bin/env bash

set -ex

rm -r -f $PWD/out/implementations/Dpt_A/
rm -r -f $PWD/out/implementations/full
rm -r -f $PWD/out/implementations/reclass
rm -r -f $PWD/out/implementations/spa-implementation
rm -r -f $PWD/out/implementations/data_output

lb workspaces | xargs lb delete || true
lb web-server unload-services

mkdir $PWD/out/implementations/common || true
cp -r $PWD/out/workspaces/stage.tgz $PWD/out/implementations/common/

tar xvf $PWD/out/implementations/spa-implementation.tgz -C $PWD/out/implementations

cp -r $PWD/data/processed_data/lb-jobs-input/* $PWD/out/implementations/spa-implementation

chmod 775 -R $PWD/out/implementations/
pushd $PWD/out/implementations/spa-implementation
./run .. ./data_output dev
popd
