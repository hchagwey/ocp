#!/usr/bin/env bash

set -ex

# This script is called from systemd timers defined in nix/timers.nix with arguments to start batch workflows!
# Arguments:
#   BATCHMONITOR - Nix arg "batchMonitors" boolean converted to string. | "true","false"
#   BUCKET - Nix arg "data_root". | string
#   DATE - Date fetched in service. | "date +%Y%m%d"
#   WORKFLOWNAME - Name of workflow to start. | "daily","weekly"

OPTS=`getopt -o b:l:d:w: --long export:,BATCHMONITOR:,BUCKET:,DATE:,WORKFLOWNAME: -n 'parse-options' -- "$@"`

if [[ $? != 0 ]] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi

echo "$OPTS"
eval set -- "${OPTS}"

BATCHMONITOR="false"
WORKFLOWNAME=""

while true;
do
   case $1 in
   -w  | --workflowname )  WORKFLOWNAME=$2; shift 2;;
   -b  | --batchmonitor )  BATCHMONITOR="true"; shift 2;;
   -l  | --location     )  BUCKET=$2; shift 2;;
   -d  | --date         )  DATE=$2; shift 2;;
    -- ) shift; break ;;
    * ) break ;;
    esac
done

# Check if other workflow is running.
if lb workflow status --active-roots | grep -Ei 'daily|weekly|bootstrap_app'; then
    echo "$(date): ERROR: Incompatible workflow is running.";
    exit 1;
fi

if [[ ! -z ${BUCKET} ]] && [[ "${BUCKET}" != s3://* ]]; then
    echo "ERROR: Bucket arg not set correctly.";
    exit 1;
else 
    lb exec /workflow '^workflow:config:location[]= "'$BUCKET'".'
fi

if [[ ! -z ${DATE} ]]; then
    lb exec /workflow '^workflow:config:current_day[]= "'$DATE'".'
fi

# Start the workflow.
lb workflow start --name $WORKFLOWNAME

# Check if batch monitoring is set to true.
if [ "${BATCHMONITOR}" == "true" ]; then
    s3_bucket=$(echo $BUCKET | cut -c 6-)
    bash /data/lb_deployment/aps/batchmonitor/batch.sh MFP_BASE $s3_bucket $WORKFLOWNAME
fi

exit 0