#!/usr/bin/env bash


######################################
# Workaround script to get local build
# dependecies when Nix-shell stops
# working :)
######################################

set -e

LB_VERSION="integration"

# Builder URL
BUILDER_URL="https://bob.logicblox.com"

# Determine OS/architecture
arch=$(uname | tr '[:upper:]' '[:lower:]')
echo "Current architecture: ${arch}"

if [[ $arch == 'linux' ]]; then
    LOGICBLOX_RELEASE=$BUILDER_URL/job/logicblox-40/$LB_VERSION/linux.binary_tarball/latest/download-by-type/file/binary-dist
else
    LOGICBLOX_RELEASE=$BUILDER_URL/job/logicblox-40/$LB_VERSION/osx.binary_tarball/latest/download-by-type/file/binary-dist
fi
echo $LOGICBLOX_RELEASE

# GBM
GBM_URL="${BUILDER_URL}/job/global-batch-manager/integration/release/latest/download/2"

function clean()
{
  if [ -d upstream ]
  then
    mkdir -p /tmp/upstream
    BACKDIR="/tmp/upstream/tgt_upstream_`date +%Y%m%d_%M%S`"
    echo -e "-------------------------"
    echo -e "Backing up downloads to $BACKDIR ..."
    echo -e "-------------------------"
    chmod -R +w upstream
    mv upstream $BACKDIR
  fi
}

function create_upstream()
{
  if [ ! -d upstream ]
  then
    mkdir -p upstream/downloads
  fi
}

function logicblox()
{
  create_upstream
  pushd upstream 1>/dev/null
  echo -e "--------------------------------"
  echo -e " Downloading logicblox release..."
  echo -e "--------------------------------"
  curl -L $LOGICBLOX_RELEASE > downloads/LogicBlox.tar.gz
  tar xfz downloads/LogicBlox.tar.gz
  rm -rf logicblox
  if [ -d LogicBlox* ]
  then
    mv LogicBlox* logicblox
  else
    mv logicblox* logicblox
  fi
  popd 1>/dev/null
}

function gbm()
{
  create_upstream
  pushd upstream 1>/dev/null
  test -d gbm && rm -rf gbm
  echo -e "+-----------------------------------------------------------------------------+"
  echo -e "| Downloading GBM...                                                    |"
  echo -e "+-----------------------------------------------------------------------------+"
  curl -L $GBM_URL > downloads/gbm.tar.gz
  mkdir -p gbm && tar xfz downloads/gbm.tar.gz -C gbm
  popd 1>/dev/null
  rm -f gbm
  ln -s upstream/gbm gbm
}

function packages()
{
  rm -rf node_modules
  echo
  echo -e "----------------------------------"
  echo -e " Getting NPM dependencies..."
  echo -e "----------------------------------"
  npm install
}

# Just a workaround for now to get connectblox services loaded.
function connectblox_services()
{
    echo '[workspace:connectblox]
        service_import = config/import-workspace.json
        service_exec = config/exec.json
        service_exec_inactive = config/exec-inactive.json
        service_delete_ws = config/delete-workspace.json' >> upstream/logicblox/config/lb-web-server.config
    lb services restart
}

#-------------------------------------------
# Main
#-------------------------------------------
if [ "$#" = "0" ]
then
  clean
  create_upstream
  logicblox
  gbm
  packages
else
  for f in $* ; do
    $f
  done
fi

source scripts/env.sh

connectblox_services

exit 0
