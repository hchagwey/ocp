#! /bin/sh
set -e

echo "+----------------------------------------------------------------------------+"
echo "| Preparation of the build env of the Starter Planning App (in 2 steps)      |"
echo "+----------------------------------------------------------------------------+"
echo "| 1/2 - Install Nix Package Manager if needed ...                            |"
echo "+----------------------------------------------------------------------------+"
nix-env --version >/dev/null 2>&1 || {
  echo >&2 "| I require Nix but it's not installed. Installing Nix Package Manager ..."
  curl https://nixos.org/nix/install | sh
  echo
  sleep 2
}
. ~/.nix-profile/etc/profile.d/nix.sh
if [[ `nix-instantiate --eval -E '(import <nixpkgs> {}).lib.nixpkgsVersion' | xargs` == 19.03* ]]
then
  echo "| Installed Nix version = 19.03"
else
  echo "Updating to 19.03 packages ..."
  nix-channel --remove `nix-channel --list | cut -d ' ' -f1`
  nix-channel --add https://nixos.org/channels/nixos-19.03 nixpkgs
  nix-channel --update
fi
if sudo grep -q trusted-users "/etc/nix/nix.conf"; then
  echo "| nix.conf file is set"
else
  echo "Updating nix.conf"
  sudo mkdir -p /etc/nix
  sudo touch /etc/nix/nix.conf
  echo "trusted-users = root $USER" | sudo tee -a /etc/nix/nix.conf
  echo "require-sigs = false"       | sudo tee -a /etc/nix/nix.conf
  sudo pkill nix-daemon || echo "Nix daemon restartd"
fi

echo "+----------------------------------------------------------------------------+"
echo "| 2/2 - Prepare build envionment (you will enter the Nix Shell) ...          |"
echo "+----------------------------------------------------------------------------+"
nix-shell -A build --option extra-binary-caches s3://logicblox-cache
