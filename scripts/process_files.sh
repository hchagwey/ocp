#!/usr/bin/env bash

###### This script is used to process the incoming files from a client ######

set -e

upload_to_s3() {
  cloud-store upload -i $1 $2 --recursive
}

download_from_s3() {
  cloud-store download -r $1 -o $2 --keydir /home/logicblox/.s3lib-keys
}

create_tmp_directories() {
  mkdir -p $raw_data
  mkdir -p $processed
  mkdir -p $unzipped_files
  mkdir -p $log_dir                  #needs root permissions
}

cleanup() {
  rm -rf $raw_data
  rm -rf $processed
  rm -rf $unzipped_files
  rm -rf $log_dir
}

unzip_files() {
  cp -r $raw_data/* $unzipped_files
  pushd $unzipped_files
  gunzip *.gz || echo "No gzipped files"
  popd
}

clean_zip_files() {
  gzip $unzipped_files/*.*
  cp -r $unzipped_files/* $processed
}

check_fdvs() {
  current_dir=$PWD
  source $SPA_HOME/scripts/check_fdvs.sh --files-list $SPA_HOME/scripts/file_list.csv --data-path $unzipped_files --log_file $fdv_log_file --clean-data
  cd $current_dir
  report_logs $fdv_log_file
}

report_logs() {
  file=$1
  if [ -s "$file" ]; then
    upload_to_s3 $file $s3_dest/logs/
    rm -rf $file
  fi
}

######### main #########

### variables

data_prefix="$1" #includes date, e.g. s3://services-spa/processed_data/20200509
s3_incom="$data_prefix"
s3_archive="$s3_incom/archive"
s3_dest="$data_prefix/"
raw_data="/tmp/raw_data/"
unzipped_files="/tmp/unzipped_files/"
processed="/tmp/processed/"
log_dir="/tmp/logs/"
fdv_log_file=$log_dir"fdvs.log"

cleanup

create_tmp_directories

echo "downloading raw files from s3 ..."

download_from_s3 "$s3_incom/" $raw_data
cloud-store copy -r "$s3_incom/" "$s3_archive/" #Archive files

unzip_files

# Checking fdvs
echo "checking fdvs ..."
check_fdvs
clean_zip_files

echo "uploading processed files to s3 ..."
upload_to_s3 $processed $s3_dest

cleanup
