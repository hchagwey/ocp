{ enableTimers ? false
, weeklyStartTime ? "Tue, 16:40"
, dailyStartTime ? "Mon-Fri *-*-* 17:00"
, ...
}:
{
  defaults = 
  { pkgs, ... }:
  {
    time.timeZone = pkgs.lib.mkOverride 0 "EST5EDT";
  };

  frontend = 
  { pkgs, ... }:
  { 
    #checker
    systemd.services.check-timers = if enableTimers then {
      description = "check misc StartTime values";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      script = ''
          source /etc/profile;
          systemd-analyze calendar "${weeklyStartTime}"
          systemd-analyze calendar "${dailyStartTime}"
        '';
      serviceConfig = {
        User = "logicblox";
        Group = "logicblox";
        Type = "oneshot";
      };
    } else {};

    ## WEEKLY ##
    systemd.services.weekly = if enableTimers then {
      description = "Weekly batch";
      script = ''
        source /etc/profile
        echo "START WEEKLY BATCH"
        lb workflow start --name weekly
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "logicblox";
        Group = "logicblox";
      };
      startAt = weeklyStartTime;
    } else {};

    ## DAILY ##
    systemd.services.daily = if enableTimers then {
      description = "Daily batch";
      script = ''
        source /etc/profile
        echo "START DAILY BATCH"
        lb workflow start --name daily
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "logicblox";
        Group = "logicblox";
      };
      startAt = dailyStartTime;
    } else {};
  };
}
