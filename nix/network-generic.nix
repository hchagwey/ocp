{ adminHost ? "localhost"
, alias_idp ? if production then "prod.???" else "tobedetermined"
, alias_sp ?  if production then "???.prod.retail.infor.com" else "spa-mfp.dev.retail.infor.com"
, url ? if production then "???.retail.infor.com" else ""
, idp ? if production then "tobedetermined" else "tobedetermined"
, allowedGroups ? if production then [ "spa-prod" ] else [ "spa-dev" ]
, dataInstallFlags ? "-i"
, frontendInstallFlags ? "-biw"
, gen_zero ? "20200425"
, includeStage ? false
, production ? false
, enableOssec ? production
, s3_dir ? null
, stageHost ? "localhost"
, workflowConfigFile ? null
, stageInstanceType ? "r4.2xlarge"
, frontendInstanceType ? "r4.xlarge"
, dataInstanceType ? "r4.2xlarge"
, steveService ? "https://steve-dev.logicblox.com/job"
, steveUser ? "spa-dev"
, steveKey ? "/run/logicblox-keys/spa-lb-steve.pem"
, ...
}:
let
  installer = builtins.storePath <installer>;

  # Ossec configuration
  ossecConfig = if (enableOssec) then { services.ossec-lb.enable = enableOssec;} else {};
in
{
  require = [
    <lbdevops/nixops/partitioned/network.nix>
    <lbdevops/nixops/generic/monitoring.nix>
  ];

  defaults =
    { config, lib, name,... }:
    {
      imports = [
        <lbdevops/nixos/local-modules/freeipa.nix>
      ] ++ (if (enableOssec) then [
        <lbdevops/nixos/local-modules/ossec-hids.nix>
        ossecConfig
      ] else []);

      freeipa.enable = true;
      freeipa.allowedGroups = allowedGroups;
      freeipa.caCertificate = <global_creds/freeipa-creds/ca.crt>;
      freeipa.tlsCertificatePem = <global_creds/freeipa-creds/ldap_tls.pem>;
      freeipa.tlsCertificateKey = <global_creds/freeipa-creds/ldap_tls.key>;

      deployment.keys.ossecKey.text = if enableOssec then builtins.readFile (<global_creds/ossec> + "/ossec-${config.deployment.name}-${name}.key") else "";

      systemd.services.lb-web-server.serviceConfig.LimitNOFILE = 100000;

      services.logicblox.config.lb-web-server = ''
        [workspace:connectblox]
        service_import = ${installer}/config/import-workspace.json
        service_exec = ${installer}/config/exec.json
        service_exec_inactive = ${installer}/config/exec-inactive.json
        service_delete_ws = ${installer}/config/delete-workspace.json
      '';

      deployment.ec2.instanceType = lib.mkOverride 1 "${dataInstanceType}";

      logicblox.application.installFlags = lib.mkOverride 10 "${dataInstallFlags}";
    };

  frontend =
    { lib, ... }:
      let
        workflowConfig = if (workflowConfigFile != null) then { source = workflowConfigFile; } else { text = ""; };
      in
    {
      imports = [
        <lbdevops/nixops/modeler-app/modeler-config.nix>
        <gbm/gbm.nix>
      ];


      #----- SSO setting -----#
      # To uncomment later when we generate these keys otherwise the deployment will fail
      # deployment.keys."${alias_idp}.cer".text = builtins.readFile ( builtins.toPath( builtins.toString(<global_creds>) + "/${alias_idp}.cer"));
      # deployment.keys."${alias_sp}.cer".text  = builtins.readFile ( builtins.toPath( builtins.toString(<global_creds>) + "/${alias_sp}.cer"));
      # deployment.keys."${alias_sp}.pem".text  = builtins.readFile ( builtins.toPath( builtins.toString(<global_creds>) + "/${alias_sp}.pem"));

      # systemd.services.sso-keys = {
      #   description = "S3 keys";
      #   before = [ "lb.target"];
      #   after = [ "keys.target"];
      #   wants = [ "keys.target" ];
      #   wantedBy = [ "lb.target" ];
      #   script = ''
      #     mkdir -p /run/logicblox-keys
      #     cp /run/keys/${alias_sp}.cer /run/logicblox-keys/
      #     cp /run/keys/${alias_sp}.pem /run/logicblox-keys/
      #     cp /run/keys/${alias_idp}.cer /run/logicblox-keys/
      #     chown -R logicblox:logicblox /run/logicblox-keys
      #   '';
      #   serviceConfig = {
      #     Type = "oneshot";
      #     RemainAfterExit = true;
      #   };
      # };

      #----- Assuming we will be using clients emailadress to authenticate -----#
      #services.logicblox.config.lb-web-server = ''
        #[saml:spa-mfp-dev]
        #attribute_map =          uid http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress
        #force_authn =            true
        #include_name_id_policy = true
        #keydir =                 /run/logicblox-keys/
        #log_level =              debug
        #meta =                   /sso/metadata
        #realm =                  spa-proxy-realm
        #redirect =               /
        #request =                /sso/request
        #response =               /sso/response

        # Service Provider
        #alias_sp =                   ${alias_sp}
        #assertion_consumer_service = https://${url}/sso/response
        #entity_id_sp =               ${url}

        # Indentity Provider
        #alias_idp =      ${alias_idp}
        #entity_id_idp =  http://${idp}/adfs/services/trust
        #signature_algo = sha256
        #sso_service =    https://${idp}/adfs/ls/
      #'';
      
      # This will enable gbm
      services.logicblox.gbm.enable = true;
      services.logicblox.config.lb-steve-client = lib.mkForce ''
        service=${steveService}
        
        [auth]
        user = ${steveUser}
        key_file = ${steveKey}
      '';

      environment.etc."logicblox/workflowConfig.logic" = workflowConfig;

      deployment.ec2.instanceType = lib.mkOverride 0 "${frontendInstanceType}";

      logicblox.application.installFlags = lib.mkOverride 0 "${frontendInstallFlags}";

      services.zabbixAgent.extraConfig = ''
        UserParameter=active.users,/run/current-system/sw/bin/grep "GET /proxy/workbooks" /data/lb_deployment/logs/current/lb-web-server-access-$(date +"%Y_%m_%d").log | /run/current-system/sw/bin/grep -o user_id=.* | /run/current-system/sw/bin/sed -e 's/user_id=//g' | /run/current-system/sw/bin/awk -F '&' '{print $1}' | /run/current-system/sw/bin/awk -F ' ' '{print $1}' | /run/current-system/sw/bin/sort -u | /run/current-system/sw/bin/grep -v infor_admin | /run/current-system/sw/bin/wc -l
      '';
    };
}

 //( if includeStage then {
  stage =
    { lib, ...}:
    {
      logicblox.application.installFlags = lib.mkOverride 0 "${dataInstallFlags}";

      deployment.ec2.instanceType = lib.mkOverride 0 "${stageInstanceType}";
    };
} else {})
