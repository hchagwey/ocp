{ installWorkflows ? true
, current_day ? null
, copy_test_data ? false
, test_data_source ? ""
, data_root ? null
, ...
}:
let
  installer = builtins.storePath <installer>;
in
{
  frontend = { pkgs, lib, config, ... }:
  let 
    workflowBindings = {
      "workflow:config:workspaces" = "/data/lb_deployment/uncompressed-workspaces";
      "workflow:config:config_dir" = "/data/lb_deployment/installed-app/config";
    };

  in
  {
    services.logicblox.lbWorkflowWorkspaces = [ "/workflow" ];
    lb-workflow.statusUrls = lib.mkOverride 0 [ "http://localhost:55183/status" ];
    
    systemd.services.workflow-bindings-setup = {
      description = "setup values of workflow predicate bindings";
      after = [ "install-app.service" ];
      wantedBy = [ "multi-user.target" ];
      script = ''
          source /etc/profile;

        '' +
          # Build a set of instructions to populate workflow bindings from workflowBindings attrSet
          builtins.concatStringsSep "\n" (pkgs.lib.mapAttrsToList (name: value: "lb exec /workflow '^${name}[]=\"${value}\".'") workflowBindings)
        ;

      serviceConfig = {
        User = "logicblox";
        Group = "logicblox";
        Type = "oneshot";
      };
    };

    systemd.services.copy-test-data = if copy_test_data then {
      description = "copy test data needed for bootstrap_app workflow";
      after = [ "install-app.service" ];
      requires = [ "workflow-bindings-setup.service" ];
      wantedBy = [ "multi-user.target" ];
      script = ''
          source /etc/profile
          $SPA_HOME/scripts/copy-test-data.sh -s=${test_data_source} -d=${data_root} -c=${current_day} -t=initial
        '';
      serviceConfig = {
        User = "logicblox";
        Group = "logicblox";
        Type = "oneshot";
      };
    } else {};    

    systemd.services.workflow-workspace-rebuild = {
      description = "rebuild workflow workspace";
      before = [ "workflow-bindings-setup.service" ];
      requires = [ "workflow-bindings-setup.service" ];
      script = ''
          source /etc/profile
          lb web-server unload-services -w /workflow
          lb import-workspace --overwrite "/workflow" "/data/lb_deployment/uncompressed-workspaces/workflow"
          lb web-server load-services -w /workflow
          echo "[INFO] workflow workspace rebuilt"
        '';

      serviceConfig = {
        User = "logicblox";
        Group = "logicblox";
        Type = "oneshot";
      };
    };
  };
}
