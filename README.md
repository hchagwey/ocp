# STARTER PLANNING APPLICATION #

This is a basic staging project structure for the starter planning application.

## DEV ENVIRONMENT - MANUAL STEPS ##

### SET UP NIX ###

Install and setup nix ( If you already have nix installed, you can skip this step )
```
curl https://nixos.org/nix/install | sh
```
Add this command to your bashrc: 
```
 . ~/.nix-profile/etc/profile.d/nix.sh
```

It is best to be using the same version of the Nix Packages collection. Currently, we are using version 19.03.
To check the version that you're using, run the following:
```
  nix-instantiate --eval '<nixpkgs>' -A lib.version
```
If you are not using the version 19.03, you can update that by running the following commands:
```
  nix-channel --remove `nix-channel --list | cut -d ' ' -f1`
  nix-channel --add https://nixos.org/channels/nixos-19.03 nixpkgs
  nix-channel --update
```

If you have freshly installed Nix, you may have to add your user to the list of trusted users.

First create the folder 
```
  sudo mkdir -p /etc/nix
  sudo touch /etc/nix/nix.conf
```

For that, you may want to run the below commands:
```
  echo "trusted-users = root $USER" | sudo tee -a "/etc/nix/nix.conf"
  sudo pkill nix-daemon
```

You will need to run the below command to trust unsigned substituters
```
  echo "require-sigs = false" | sudo tee -a "/etc/nix/nix.conf"
  sudo pkill nix-daemon
```

Second, it will make your builds more reliable to add max-jobs = 1 to the file to prevent concurrent jobs from both
trying to control LB server state.

## Full local build ##

**AWS credentials**

Run the command `hologram me` to make sure that your aws credentials are all setup.
If you don't alreay have hologram, check this link https://github.com/AdRoll/hologram/releases

You can also request, through an OPS Jira ticket, a Read Only access to the buckets logicblox-private and logicblox-cache.
In that case, you **do not** have to install hologram.

**Build**

Run the command in order to bring all the needed dependencies along with the dev-data
```
nix-shell -A build -I nixpkgs=channel:nixos-19.03 --option extra-binary-caches s3://logicblox-cache
```
Once the nix shell is ready, you can build the project by running the below commands :
```
lb services start
```
```
 lb config
```
```
 make
```
```
 make install
```
**Deploy**

You cannot deploy SPA without building mfp_base or mfp_hybrid project. In fact, to be able to run staging workflows we need to deploy /workflow workspace first which is done as part of the IDE build. Please take a look at the IDE documentation https://developer.logicblox.com/content/docs4/ide-reference/webhelp/build.html (3rd section) for further information.

Once the /workflow workspace is up, run the following command to bootstrap staging.
```
make run-workflow-bootstrap-stage
```
## Main workflows ##

**Initiation Batches**
The **bootstrap_stage** is important for: 

* Bootstrap GBM : Just after deploying an environment, this step is run in order to setup the Generation Zero, set the data prefix for data imports and exports, and load the LB web server config. 
* Deploy /stage workspace
* Import history data : Importing 2 years of full data in the system.
* Export history data

By the end of the bootsrap workflow, the system contains the following workspaces: 
* /stage
* /workflow

**Weekly Stage Batch**
The **weekly_stage** step is important for: 

* Imports of daily data
* Aggregations
* Reclassification
* Export of Daily data
* backup

**Daily Stage Batch**
The **daily_stage** step is important for: 

* Imports of daily data
* Aggregations
* Export of daily data

### Modifying the project ###

**src directory**

* src/config/ : configuration specification for levels, dimensions, hierarchies, and measures. These files are used to
generate LogiQL declarations of predicates that contain level members, their mappings, as well as measure base predicates
generate LogiQL that populates the measure meta model accordingly.

* src/scripts : utility scripts, including one that generates LogiQL from metamodel specification, as well as helper scripts to inspect the model for debugging

* src/logiql/ : contains both hand-written as well as generated LogiQL (from config)

* src/logiql/stage : contains the hierarchies and data model declaration and services used for staging.

**nix directory**

* nix/network.nix : this file contains both the logical and physical description of the machine(s) to be deployed.

* nix/workflows.nix : this file contains the logic for rebuilding the workflow workspace as well as any parameters that need to be included in the workflow workspace during deployment.

* nix/timers.nix : this file contains a list of workflows as systemd services, as well as timers that automate the running of these workflows.

**workflows directory**

* workflows/ : contains the main workflows, such as bootstraps and dailies. Also contains ooc workflows such as backups and recovery.
* workflows/lib : contains modular workflows used in the main workflows. 

**Local workflow make targets**
* To bootstrap GBM:
```
make clean && lb config && make run-workflow-bootstrap-gbm
```
* To just archive a stage workspace and bootstrap it:
```
make clean && lb config && make run-workflow-bootstrap-stage
```
* After bootstrap, to run the daily stage workflow:
```
make run-workflow-daily-stage
```
* After bootstrap, to run the weekly stage workflow:
```
make run-workflow-weekly-stage
```
* Recover a stage workspace:
```
make run-workflow-recover-stage
```
* Backup stage workspace:
```
make run-workflow-backup-stage
```