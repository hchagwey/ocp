from lbconfig.api import *
from lbconfig.modeler import *
import json
import os

# TODO - Edit stage_name (name of the staging workspace).
stage_name = 'stage'
stage_library = stage_name
stage_run_ws = '/' + stage_name

# TODO - Append list of workflows to install.
# The name of the workflow here needs to match
# the name of the worflow file.
# (src/workflows/{workflow_name}.wf)
installed_workflows = [
    'bootstrap_gbm',
    'bootstrap_stage',
    'daily_stage',
    'weekly_stage'
]

basedir = os.path.dirname(os.path.realpath(__file__))

# -----------------------------
# - Libraries and dependecies -
# -----------------------------
depends_on(
    lb_web_dep,
    logicblox_dep,
    app_home='$(APP_HOME)',
    gbm='$(GBM_HOME)',
    lb_measure_service='$(LB_MEASURE_SERVICE_HOME)',
    lb_web_workbooks='$(LB_WORKBOOK_SERVICE_HOME)',
    lb_workflow='$(LB_WORKFLOW_HOME)',
    modelerjs='$(LOGICBLOX_HOME)'
)

targets = [
    'archive-ws-/%s' % stage_name,
    'archive-ws-/workflow',
    'lb-libraries'
]

lbconfig_package(stage_name, default_prefix='out', default_targets=targets)

# ----------------------
# - Stage WS Libraries -
# ----------------------
libraries_stage = [stage_library]

for lib in libraries_stage:
    lb_library(
        name=lib,
        srcdir='src/logiql',
        generated=False,
        deps={
            'lb_web': '$(lb_web)',
            'lb_measure_service': '$(lb_measure_service)'
        }
    )

# -----------------
# - Archive Stage -
# -----------------
ws_archive(
    name=stage_run_ws,
    libraries=[stage_library],
    compress=True,
    keep=True
)

rule(
    output='$(build)/workspaces%s.tgz' % stage_name,
    input=[archive_export_target(stage_name)],
    commands=[
        'tar cfz $(build)/workspaces%s.tgz %s' %
        (stage_name,
         archive_export_target(stage_name))
    ]
)

# ------------
# - Workflow -
# ------------
check_lb_workspace(
    name='/workflow',
    libraries=['workflow'],
    create_cmd=
    '$(logicblox)/bin/lb workflow create --workspace /workflow --overwrite --load-services true',
    compress=True,
    archived=True,
    keep=True
)

lb_workflow_library(
    name='workflow',
    active=[],
    execute=[],
    extensions=[],
    sources_root='src/workflows',
    workflows=installed_workflows
)

#lb_workflow_doc(
#    extensions=[],
#    install=True,
#    isdir=True,
#    outdir='$(build)/doc/workflows',
#    target='workflow-doc',
#    title="SPA Workflow Documentation",
#    workflow_file='$(app_home)/src/workflows/spa.wf'
#)

rule(
    output='$(build)/workspaces%s.tgz' % 'workflow',
    input=[archive_export_target('workflow')],
    commands=[
        'tar cfz $(build)/workspaces%s.tgz %s' %
        ('workflow',
         archive_export_target('workflow'))
    ]
)

# -------------------
# - Local Workflows -
# -------------------
rule(
    output='gbm-restart',
    input=[],
    commands=['bash $(gbm)/gbm.sh restart $(app_home)/config/gbm.config']
)

rule(
    output='gbm-stop',
    input=[],
    commands=['bash $(gbm)/gbm.sh stop']
)

lb_workflow_run(
    workflow_file="$(app_home)/src/workflows/bootstrap_gbm.wf",
    name="bootstrap-gbm",
    params=['config_dir=$(topdir)/config',
            'gen_zero=20200425',
            'location=$(PWD)/data/processed_data',
            'netmap_root=$(PWD)/data/processed_data/config',
            'key=starter-planning-app-test-key'
    ]
)

lb_workflow_run(
    workflow_file="$(app_home)/src/workflows/bootstrap_stage.wf",
    name="bootstrap-stage",
    targets=[
        'archive-ws-/%s' % stage_name,
        'gbm-restart',
        'run-workflow-bootstrap-gbm'
    ],
    init=[
        'mkdir -p $(build)/workspaces/%s && tar xvfz $(build)/workspaces/%s.tgz -C $(build)/workspaces/%s'
        % (stage_name,
           stage_name,
           stage_name)
    ],
    params=['current_day=20200425',
            'first_week=2020_Wk_13',
            'key=starter-planning-app-test-key',
            'location=$(PWD)/data/processed_data',
            'scope=stage',
            'stage_workspace_name=stage',
            'workspaces=$(build)/workspaces'
    ]
)

lb_workflow_run(
    workflow_file="$(app_home)/src/workflows/weekly_stage.wf",
    name="weekly-stage",
    targets=[],
    init=[
        'mkdir -p $(build)/workspaces/%s && tar xvfz $(build)/workspaces/%s.tgz -C $(build)/workspaces/%s'
        % (stage_name,
           stage_name,
           stage_name)
    ],
    params=['key=starter-planning-app-test-key',
            'location=$(PWD)/data/processed_data',
            'scope=stage',
            'stage_workspace_name=stage',
            'workspaces=$(build)/workspaces'
    ]
)

lb_workflow_run(
    workflow_file="$(app_home)/src/workflows/daily_stage.wf",
    name="daily-stage",
    targets=[],
    init=[
        'mkdir -p $(build)/workspaces/%s && tar xvfz $(build)/workspaces/%s.tgz -C $(build)/workspaces/%s'
        % (stage_name,
           stage_name,
           stage_name)
    ],
    bindings=['workflow:config:key=starter-planning-app-test-key',
            'workflow:config:location=$(PWD)/data/processed_data',
            'workflow:config:stage_workspace_name=stage'
    ],
    params=['scope=stage']
)

lb_workflow_run(
    workflow_file="$(app_home)/src/workflows/recover_stage.wf",
    name="recover-stage",
    targets=[],
    init=[
        'mkdir -p $(build)/workspaces/%s && tar xvfz $(build)/workspaces/%s.tgz -C $(build)/workspaces/%s'
        % (stage_name,
           stage_name,
           stage_name)
    ],
    bindings=['workflow:config:gen_zero=20200425',
            'workflow:config:key=starter-planning-app-test-key',
            'workflow:config:location=$(PWD)/data/processed_data',
            'workflow:config:config_dir=$(topdir)/config',
            'workflow:config:netmap_root=$(PWD)/data/processed_data/config',
            'workflow:config:current_day=20200425',
            'workflow:config:scope=stage',
            'workflow:config:stage_workspace_name=stage',
            'workflow:config:stage_batch_tag_restore=stage-restore-cost-measures',
            'workflow:config:blocks_agg_name=stage:aggregations:MFP_PL_aggs',
            'workflow:config:workspaces=$(build)/workspaces'
    ],
    params=['scope=stage']
)

lb_workflow_run(
    workflow_file="$(app_home)/src/workflows/backup_stage.wf",
    name="backup-stage",
    targets=[],
    init=[
        'mkdir -p $(build)/workspaces/%s && tar xvfz $(build)/workspaces/%s.tgz -C $(build)/workspaces/%s'
        % (stage_name,
           stage_name,
           stage_name)
    ],
    params=['current_day=20200425',
            'stage_batch_tag_backup=stage-backup-cost-measures',
            'key=starter-planning-app-test-key',
            'scope=stage'
    ]
)

# -----------
# - Install -
# -----------
install_dir('$(build)/workspaces', 'workspaces')
install_dir('$(gbm)', 'gbm')
install_dir('config', 'config')
install_dir('nix', 'nix')
install_dir('scripts', 'scripts')
install_dir('src', 'src')
install_file('install.sh', '')

def refresh_services(ws):
    return [
        'lb web-server unload-services -w ' + ws,
        'lb web-server load-services -w ' + ws
    ]


# 5- Deploy (build workspaces)

rule(
    output='$(build)/.$(build)/.refresh-services',
    input=[check_ws_success_file(stage_run_ws),
           '$(build)/.load-handlers'],
    commands=refresh_services(stage_run_ws)
)

rule(
    output='$(build)/.$(build)/.refresh-services',
    input=[check_ws_success_file("/workflow"),
           '$(build)/.load-handlers'],
    commands=refresh_services("/workflow")
)


def lb_web_server_pid_file():
    return os.getenv(
        'LB_DEPLOYMENT_HOME',
        '~/lb_deployment'
    ) + '/logs/current/lb-web-server.pid'


rule(
    output='$(build)/.load-handlers',
    input=[lb_web_server_pid_file()],
    commands=[
        'lb web-server load --jar $(lb_web)/lib/java/handlers/bloxweb-connectblox.jar',
        'lb web-server load --jar $(lb_measure_service)/lib/java/handlers/lb-measure-service.jar',
        'lb web-server load --jar $(lb_web_workbooks)/lib/java/lb-web-workbooks.jar',
        'lb web-server load --jar $(lb_web)/lib/java/handlers/lb-web-json.jar',
        'touch $(build)/.load-handlers'
    ]
)

# -----------------------
# - Standard make rules -
#   make clean
# -----------------------
rule(
    output='clean',
    input=[],
    commands=[
        'rm -rf build',
        'lb web-server unload-services -w %s || echo No %s workspace' %
        (stage_run_ws,
         stage_run_ws),
        'lb web-server unload-services -w %s || echo No %s workspace' %
        ('/workflow',
         '/workflow')
    ]
)

# Use YAPF to format python files
# Skip modeler_extension.py since we intend to delete it
rule(
    output='yapf',
    input=[],
    commands='yapf --in-place config.py loadCubiql.py',
    phony=True
)
